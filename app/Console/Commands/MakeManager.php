<?php

namespace App\Console\Commands;

use App\Traits\UtilCommandMake;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeManager extends Command
{
    use UtilCommandMake;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:manager {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make and add Manager class';
    protected Filesystem $files;

    private string $_type = 'Manager';
    private string $_name_postfix;
    private string $_output_path;

    private array $_stub_custom_values = [];


    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->_make_model();
        $this->_make_service();
        $this->_make_provider();
        $this->_make_facade();
        $this->_make_test();

        $this->_inform();
    }

    private function _make_model()
    {
        $this->_name_postfix = '';
        $this->_output_path = 'app/Models/';
        $this->_stub_filename = 'model';

        $this->_execute();

        $this->_stub_custom_values['model_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['model_classname'] = $this->_classname;
    }

    private function _make_service()
    {
        $this->_name_postfix = 'ManagerService';
        $this->_output_path = 'app/Services/Managers/';
        $this->_stub_filename = 'service.manager';

        $this->_execute();

        $this->_stub_custom_values['service_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['service_classname'] = $this->_classname;
        $this->_stub_custom_values['service_identity'] = $this->_stub_custom_values['identity'];
    }

    private function _make_provider()
    {
        $this->_name_postfix = 'ManagerProvider';
        $this->_output_path = 'app/Providers/Managers/';
        $this->_stub_filename = 'provider.manager';

        $this->_execute();

        $this->_stub_custom_values['provider_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_facade()
    {
        $this->_name_postfix = 'ManagerFacade';
        $this->_output_path = 'app/Facades/Managers/';
        $this->_stub_filename = 'facade.manager';

        $this->_execute();

        $this->_stub_custom_values['facade_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_test()
    {
        $this->_name_postfix = 'ManagerTest';
        $this->_output_path = 'tests/Unit/Manager/';
        $this->_stub_filename = 'test.unit.manager';

        $this->_execute();

        $this->_stub_custom_values['test_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _inform() // TODO in trait
    {
        //'BrainManager' => App\Facades\BrainManagerFacade::class,

        $this->info("** Info");
        $this->info("Edit config/app.php and add");
        $this->info($this->_stub_custom_values['provider_full_namespace'] . '::class,');
        $this->info("'$this->_name" . "Manager' => " . $this->_stub_custom_values['facade_full_namespace'] . '::class,');
        $this->info("\n");
    }
}

// refs :
