<?php

namespace App\Console\Commands;

use App\Traits\UtilCommandMake;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeRepository extends Command
{
    use UtilCommandMake;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make and add repository class';
    protected Filesystem $files;

    private string $_type = 'Repository';
    private string $_name_postfix;
    private string $_output_path;

    private array $_stub_custom_values = [];


    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->_make_model();
        $this->_make_service();
        $this->_make_provider();
        $this->_make_facade();
        $this->_make_test();

        $this->_inform();
    }

    private function _make_model()
    {
        $this->_name_postfix = '';
        $this->_output_path = 'app/Models/';
        $this->_stub_filename = 'model';

        $this->_execute();

        $this->_stub_custom_values['model_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['model_classname'] = $this->_classname;
    }

    private function _make_service()
    {
        $this->_name_postfix = 'RepositoryService';
        $this->_output_path = 'app/Services/Repositories/';
        $this->_stub_filename = 'service.repository';

        $this->_execute();

        $this->_stub_custom_values['service_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['service_classname'] = $this->_classname;
        $this->_stub_custom_values['service_identity'] = $this->_stub_custom_values['identity'];
    }

    private function _make_provider()
    {
        $this->_name_postfix = 'RepositoryProvider';
        $this->_output_path = 'app/Providers/Repositories/';
        $this->_stub_filename = 'provider.repository';

        $this->_execute();

        $this->_stub_custom_values['provider_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_facade()
    {
        $this->_name_postfix = 'RepositoryFacade';
        $this->_output_path = 'app/Facades/Repositories/';
        $this->_stub_filename = 'facade.repository';

        $this->_execute();

        $this->_stub_custom_values['facade_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_test()
    {
        $this->_name_postfix = 'RepositoryTest';
        $this->_output_path = 'tests/Unit/Repository/';
        $this->_stub_filename = 'test.unit.repository';

        $this->_execute();

        $this->_stub_custom_values['test_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _inform()
    {
        //'BrainManager' => App\Facades\BrainManagerFacade::class,

        $this->info("** Info");
        $this->info("Edit config/app.php and add");
        $this->info($this->_stub_custom_values['provider_full_namespace'] . '::class,');
        $this->info("'$this->_name" . "Repository' => " . $this->_stub_custom_values['facade_full_namespace'] . '::class,');
        $this->info("\n");
    }
}

// refs :
