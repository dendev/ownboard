<?php
namespace App\Facades\Managers\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_manager';
    }
}
