<?php
namespace App\Facades\Managers\Users;

use Illuminate\Support\Facades\Facade;


class UserManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'user_manager';
    }
}
