<?php
namespace App\Facades\Repositories\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementCategoryRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_category_repository';
    }
}
