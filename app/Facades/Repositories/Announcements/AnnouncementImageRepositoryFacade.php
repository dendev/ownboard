<?php
namespace App\Facades\Repositories\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementImageRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_image_repository';
    }
}
