<?php
namespace App\Facades\Repositories\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementLinkRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_link_repository';
    }
}
