<?php
namespace App\Facades\Repositories\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementListLinkRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_list_link_repository';
    }
}
