<?php
namespace App\Facades\Repositories\Announcements;

use Illuminate\Support\Facades\Facade;


class AnnouncementRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'announcement_repository';
    }
}
