<?php
namespace App\Facades\Repositories\Groups;

use Illuminate\Support\Facades\Facade;


class GroupCategoryRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'group_category_repository';
    }
}
