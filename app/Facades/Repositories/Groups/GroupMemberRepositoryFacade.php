<?php
namespace App\Facades\Repositories\Groups;

use Illuminate\Support\Facades\Facade;


class GroupMemberRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'group_member_repository';
    }
}
