<?php
namespace App\Facades\Repositories\Groups;

use Illuminate\Support\Facades\Facade;


class GroupRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'group_repository';
    }
}
