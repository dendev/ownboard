<?php
namespace App\Facades\Repositories\Groups;

use Illuminate\Support\Facades\Facade;


class GroupTypeRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'group_type_repository';
    }
}
