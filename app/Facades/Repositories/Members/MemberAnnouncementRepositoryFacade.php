<?php
namespace App\Facades\Repositories\Members;

use Illuminate\Support\Facades\Facade;


class MemberAnnouncementRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'member_announcement_repository';
    }
}
