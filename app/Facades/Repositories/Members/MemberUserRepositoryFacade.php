<?php
namespace App\Facades\Repositories\Members;

use Illuminate\Support\Facades\Facade;


class MemberUserRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'member_user_repository';
    }
}
