<?php
namespace App\Facades\Repositories\Users;

use Illuminate\Support\Facades\Facade;


class UserRepositoryFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'user_repository';
    }
}
