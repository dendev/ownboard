<?php

namespace App\Filament\Resources\AnnouncementCategoryResource\Pages;

use App\Filament\Resources\AnnouncementCategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementCategory extends CreateRecord
{
    protected static string $resource = AnnouncementCategoryResource::class;
}
