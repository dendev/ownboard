<?php

namespace App\Filament\Resources\AnnouncementCategoryResource\Pages;

use App\Filament\Resources\AnnouncementCategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementCategory extends EditRecord
{
    protected static string $resource = AnnouncementCategoryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
