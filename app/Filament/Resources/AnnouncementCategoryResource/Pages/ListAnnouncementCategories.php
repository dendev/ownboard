<?php

namespace App\Filament\Resources\AnnouncementCategoryResource\Pages;

use App\Filament\Resources\AnnouncementCategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAnnouncementCategories extends ListRecords
{
    protected static string $resource = AnnouncementCategoryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
