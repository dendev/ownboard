<?php

namespace App\Filament\Resources\AnnouncementHistoricStatusResource\Pages;

use App\Filament\Resources\AnnouncementHistoricStatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementHistoricStatus extends CreateRecord
{
    protected static string $resource = AnnouncementHistoricStatusResource::class;
}
