<?php

namespace App\Filament\Resources\AnnouncementHistoricStatusResource\Pages;

use App\Filament\Resources\AnnouncementHistoricStatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementHistoricStatus extends EditRecord
{
    protected static string $resource = AnnouncementHistoricStatusResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
