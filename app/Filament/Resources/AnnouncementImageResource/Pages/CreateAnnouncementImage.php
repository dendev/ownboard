<?php

namespace App\Filament\Resources\AnnouncementImageResource\Pages;

use App\Filament\Resources\AnnouncementImageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementImage extends CreateRecord
{
    protected static string $resource = AnnouncementImageResource::class;
}
