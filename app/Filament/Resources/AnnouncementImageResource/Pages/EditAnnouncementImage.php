<?php

namespace App\Filament\Resources\AnnouncementImageResource\Pages;

use App\Filament\Resources\AnnouncementImageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementImage extends EditRecord
{
    protected static string $resource = AnnouncementImageResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
