<?php

namespace App\Filament\Resources\AnnouncementImageResource\Pages;

use App\Filament\Resources\AnnouncementImageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAnnouncementImages extends ListRecords
{
    protected static string $resource = AnnouncementImageResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
