<?php

namespace App\Filament\Resources\AnnouncementLinkResource\Pages;

use App\Filament\Resources\AnnouncementLinkResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementLink extends CreateRecord
{
    protected static string $resource = AnnouncementLinkResource::class;
}
