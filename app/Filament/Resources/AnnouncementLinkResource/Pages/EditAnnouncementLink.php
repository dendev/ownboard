<?php

namespace App\Filament\Resources\AnnouncementLinkResource\Pages;

use App\Filament\Resources\AnnouncementLinkResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementLink extends EditRecord
{
    protected static string $resource = AnnouncementLinkResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
