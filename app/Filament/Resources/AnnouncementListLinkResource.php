<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AnnouncementListLinkResource\Pages;
use App\Filament\Resources\AnnouncementListLinkResource\RelationManagers;
use App\Models\Announcements\AnnouncementListLink;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class AnnouncementListLinkResource extends Resource
{
    protected static ?string $model = AnnouncementListLink::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('announcement_id')
                    ->required(),
                Forms\Components\TextInput::make('link_id')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('announcement_id'),
                Tables\Columns\TextColumn::make('link_id'),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAnnouncementListLinks::route('/'),
            'create' => Pages\CreateAnnouncementListLink::route('/create'),
            'edit' => Pages\EditAnnouncementListLink::route('/{record}/edit'),
        ];
    }
}
