<?php

namespace App\Filament\Resources\AnnouncementListLinkResource\Pages;

use App\Filament\Resources\AnnouncementListLinkResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementListLink extends CreateRecord
{
    protected static string $resource = AnnouncementListLinkResource::class;
}
