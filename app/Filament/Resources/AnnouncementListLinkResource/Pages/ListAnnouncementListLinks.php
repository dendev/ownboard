<?php

namespace App\Filament\Resources\AnnouncementListLinkResource\Pages;

use App\Filament\Resources\AnnouncementListLinkResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAnnouncementListLinks extends ListRecords
{
    protected static string $resource = AnnouncementListLinkResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
