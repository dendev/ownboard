<?php

namespace App\Filament\Resources\AnnouncementPriorityResource\Pages;

use App\Filament\Resources\AnnouncementPriorityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementPriority extends CreateRecord
{
    protected static string $resource = AnnouncementPriorityResource::class;
}
