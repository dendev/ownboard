<?php

namespace App\Filament\Resources\AnnouncementPriorityResource\Pages;

use App\Filament\Resources\AnnouncementPriorityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementPriority extends EditRecord
{
    protected static string $resource = AnnouncementPriorityResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
