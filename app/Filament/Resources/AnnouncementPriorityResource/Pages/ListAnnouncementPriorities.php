<?php

namespace App\Filament\Resources\AnnouncementPriorityResource\Pages;

use App\Filament\Resources\AnnouncementPriorityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAnnouncementPriorities extends ListRecords
{
    protected static string $resource = AnnouncementPriorityResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
