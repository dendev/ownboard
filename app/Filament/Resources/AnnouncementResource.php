<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AnnouncementResource\Pages;
use App\Filament\Resources\AnnouncementResource\RelationManagers;
use App\Models\Announcements\Announcement;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class AnnouncementResource extends Resource
{
    protected static ?string $model = Announcement::class;

    protected static ?string $navigationIcon = 'heroicon-o-newspaper';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('category_id')
                    ->required(),
                Forms\Components\TextInput::make('type_id')
                    ->required(),
                Forms\Components\TextInput::make('status_id')
                    ->required(),
                Forms\Components\TextInput::make('priority_id')
                    ->required(),
                Forms\Components\TextInput::make('image_id')
                    ->required(),
                Forms\Components\TextInput::make('creator_id')
                    ->required(),
                Forms\Components\TextInput::make('corrector_id'),
                Forms\Components\TextInput::make('suppressor_id'),
                Forms\Components\TextInput::make('label')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('identity')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Textarea::make('content')
                    ->required(),
                Forms\Components\DateTimePicker::make('begin_at')
                    ->required(),
                Forms\Components\DateTimePicker::make('end_at'),
                Forms\Components\TextInput::make('order')
                    ->required()
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('category_id'),
                Tables\Columns\TextColumn::make('type_id'),
                Tables\Columns\TextColumn::make('status_id'),
                Tables\Columns\TextColumn::make('priority_id'),
                Tables\Columns\TextColumn::make('image_id'),
                Tables\Columns\TextColumn::make('creator_id'),
                Tables\Columns\TextColumn::make('corrector_id'),
                Tables\Columns\TextColumn::make('suppressor_id'),
                Tables\Columns\TextColumn::make('label'),
                Tables\Columns\TextColumn::make('identity'),
                Tables\Columns\TextColumn::make('content'),
                Tables\Columns\TextColumn::make('begin_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('end_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('order'),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAnnouncements::route('/'),
            'create' => Pages\CreateAnnouncement::route('/create'),
            'edit' => Pages\EditAnnouncement::route('/{record}/edit'),
        ];
    }
}
