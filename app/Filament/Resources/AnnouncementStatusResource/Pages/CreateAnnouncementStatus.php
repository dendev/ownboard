<?php

namespace App\Filament\Resources\AnnouncementStatusResource\Pages;

use App\Filament\Resources\AnnouncementStatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementStatus extends CreateRecord
{
    protected static string $resource = AnnouncementStatusResource::class;
}
