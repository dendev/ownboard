<?php

namespace App\Filament\Resources\AnnouncementStatusResource\Pages;

use App\Filament\Resources\AnnouncementStatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementStatus extends EditRecord
{
    protected static string $resource = AnnouncementStatusResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
