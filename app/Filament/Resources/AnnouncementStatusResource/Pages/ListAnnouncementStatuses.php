<?php

namespace App\Filament\Resources\AnnouncementStatusResource\Pages;

use App\Filament\Resources\AnnouncementStatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAnnouncementStatuses extends ListRecords
{
    protected static string $resource = AnnouncementStatusResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
