<?php

namespace App\Filament\Resources\AnnouncementTypeResource\Pages;

use App\Filament\Resources\AnnouncementTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAnnouncementType extends CreateRecord
{
    protected static string $resource = AnnouncementTypeResource::class;
}
