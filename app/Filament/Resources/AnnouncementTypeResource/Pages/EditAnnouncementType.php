<?php

namespace App\Filament\Resources\AnnouncementTypeResource\Pages;

use App\Filament\Resources\AnnouncementTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAnnouncementType extends EditRecord
{
    protected static string $resource = AnnouncementTypeResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
