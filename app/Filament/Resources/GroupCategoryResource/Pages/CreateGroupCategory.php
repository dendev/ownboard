<?php

namespace App\Filament\Resources\GroupCategoryResource\Pages;

use App\Filament\Resources\GroupCategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGroupCategory extends CreateRecord
{
    protected static string $resource = GroupCategoryResource::class;
}
