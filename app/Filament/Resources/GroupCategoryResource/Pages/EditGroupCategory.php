<?php

namespace App\Filament\Resources\GroupCategoryResource\Pages;

use App\Filament\Resources\GroupCategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditGroupCategory extends EditRecord
{
    protected static string $resource = GroupCategoryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
