<?php

namespace App\Filament\Resources;

use App\Filament\Resources\GroupMemberResource\Pages;
use App\Filament\Resources\GroupMemberResource\RelationManagers;
use App\Models\Groups\GroupMember;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class GroupMemberResource extends Resource
{
    protected static ?string $model = GroupMember::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('group_id')
                    ->required(),
                Forms\Components\TextInput::make('member_id')
                    ->required(),
                Forms\Components\TextInput::make('member_table')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_user')
                    ->required(),
                Forms\Components\Toggle::make('user_can_view_members')
                    ->required(),
                Forms\Components\Toggle::make('user_can_remove_member')
                    ->required(),
                Forms\Components\Toggle::make('user_can_add_member')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('group_id'),
                Tables\Columns\TextColumn::make('member_id'),
                Tables\Columns\TextColumn::make('member_table'),
                Tables\Columns\IconColumn::make('is_user')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_view_members')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_remove_member')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_add_member')
                    ->boolean(),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGroupMembers::route('/'),
            'create' => Pages\CreateGroupMember::route('/create'),
            'edit' => Pages\EditGroupMember::route('/{record}/edit'),
        ];
    }
}
