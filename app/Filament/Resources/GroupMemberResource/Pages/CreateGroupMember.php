<?php

namespace App\Filament\Resources\GroupMemberResource\Pages;

use App\Filament\Resources\GroupMemberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGroupMember extends CreateRecord
{
    protected static string $resource = GroupMemberResource::class;
}
