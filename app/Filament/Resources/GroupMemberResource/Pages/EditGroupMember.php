<?php

namespace App\Filament\Resources\GroupMemberResource\Pages;

use App\Filament\Resources\GroupMemberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditGroupMember extends EditRecord
{
    protected static string $resource = GroupMemberResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
