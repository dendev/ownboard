<?php

namespace App\Filament\Resources\GroupMemberResource\Pages;

use App\Filament\Resources\GroupMemberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListGroupMembers extends ListRecords
{
    protected static string $resource = GroupMemberResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
