<?php

namespace App\Filament\Resources\GroupTypeResource\Pages;

use App\Filament\Resources\GroupTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGroupType extends CreateRecord
{
    protected static string $resource = GroupTypeResource::class;
}
