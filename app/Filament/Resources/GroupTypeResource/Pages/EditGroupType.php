<?php

namespace App\Filament\Resources\GroupTypeResource\Pages;

use App\Filament\Resources\GroupTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditGroupType extends EditRecord
{
    protected static string $resource = GroupTypeResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
