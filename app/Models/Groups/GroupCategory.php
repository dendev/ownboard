<?php

namespace App\Models\Groups;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class GroupCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'label',
        'parent_id',
        'description',
        'order'
    ];

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
     protected static function booted(): void
    {
        static::saving(function (GroupCategory $model) {
            if( is_null( $model->identity) || $model->identity === '')
                $model->identity = Str::of($model->label)->snake();

          if( is_null( $model->order) || $model->order === '')
                $model->model = ucfirst((substr(Str::of($model->label)->snake(), 0, 1)));
        });
    }
}
