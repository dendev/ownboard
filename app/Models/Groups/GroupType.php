<?php

namespace App\Models\Groups;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class GroupType extends Model
{
    use HasFactory;

    protected $fillable = [
        'label',
        'description',
        'order',
    ];

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
    protected static function booted(): void
    {
        static::saving(function (GroupType $group_type) {
            if( is_null( $group_type->identity) || $group_type->identity === '')
                $group_type->identity = Str::of($group_type->label)->snake();
        });
    }
}
