<?php

namespace App\Models\Members;

use App\Models\Groups\Group;
use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MemberAnnouncement extends GroupMember
{
    use HasFactory;

    protected $fillable = [
        'announcement_id',
        'group_id',
        'is_user',
        'user_can_view_members',
        'user_can_remove_member',
        'user_can_add_member',
    ];

    protected $table = 'member_announcements';

    public function __construct(array $attributes = [])
    {
        /*
        $attributes['member_table'] = 'announcements';

        if( array_key_exists('announcement_id', $attributes))
        {
            $attributes['member_id'] = $attributes['announcement_id'];
            unset( $attributes['announcement_id']);
        }
*/
        parent::__construct($attributes);
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function save(array $options = [])
    {
        parent::save($options);

        $this->table = 'member_announcements';
        $this->refresh();

        return $this;
    }

    public function update(array $attributes = [], array $options = [])
    {
        return parent::update($attributes, $options);
    }

    public function delete()
    {
        return parent::delete();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(User::class, 'member_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */

     protected static function booted(): void
    {
        static::saving(function (MemberAnnouncement $model) {
            // member table
            if( is_null( $model->member_table) || $model->member_table === '')
                $model->member_table = 'announcements';

            // member id
            if( array_key_exists('announcement_id', $model->attributes))
            {
                $model->attributes['member_id'] = $model->attributes['announcement_id'];
                unset( $model->attributes['announcement_id']);
            }
        });
    }
}
