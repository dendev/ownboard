<?php

namespace App\Models\Members;

use App\Models\Groups\Group;
use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberUser extends GroupMember
{
    use HasFactory;

    protected $table = 'member_users';

    public function __construct(array $attributes = [])
    {
        $attributes['member_table'] = 'users';

        if( array_key_exists('user_id', $attributes))
        {
            $attributes['member_id'] = $attributes['user_id'];
            unset( $attributes['user_id']);
        }

        parent::__construct($attributes);
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function save(array $options = [])
    {
        parent::save($options);

        $this->table = 'member_users';
        $this->refresh();

        return $this;
    }

    public function update(array $attributes = [], array $options = [])
    {
        return parent::update($attributes, $options);
    }

    public function delete()
    {
        return parent::delete();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'member_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */

    /*
    private function _parent_to_child(GroupMember $parent)
    {
        $classname = self::class;
        $child = $classname($parent->toArray());
    }
    */
}
