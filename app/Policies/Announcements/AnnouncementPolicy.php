<?php

namespace App\Policies\Announcements;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class AnnouncementPolicy
{
 use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_create'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_update'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_delete'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_show'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_search'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_search', 'User', $user);

        return $can;
    }
}
