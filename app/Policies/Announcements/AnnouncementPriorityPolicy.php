<?php

namespace App\Policies\Announcements;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class AnnouncementPriorityPolicy
{
 use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_priority_create'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_priority_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_priority_update'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_priority_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_priority_delete'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_priority_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_priority_show'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_priority_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('announcement_priority_search'))
            $can = true;

        if( ! $can )
            $this->_log('announcement_priority_search', 'User', $user);

        return $can;
    }
}
