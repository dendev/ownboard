<?php

namespace App\Policies\Groups;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class GroupCategoryPolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_category_create'))
            $can = true;

        if( ! $can )
            $this->_log('group_category_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_category_update'))
            $can = true;

        if( ! $can )
            $this->_log('group_category_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_category_delete'))
            $can = true;

        if( ! $can )
            $this->_log('group_category_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_category_show'))
            $can = true;

        if( ! $can )
            $this->_log('group_category_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_category_search'))
            $can = true;

        if( ! $can )
            $this->_log('group_category_search', 'User', $user);

        return $can;
    }
}
