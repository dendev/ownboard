<?php

namespace App\Policies\Groups;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class GroupPolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_create'))
            $can = true;

        if( ! $can )
            $this->_log('group_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_update'))
            $can = true;

        if( ! $can )
            $this->_log('group_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_delete'))
            $can = true;

        if( ! $can )
            $this->_log('group_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_show'))
            $can = true;

        if( ! $can )
            $this->_log('group_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_search'))
            $can = true;

        if( ! $can )
            $this->_log('group_search', 'User', $user);

        return $can;
    }
}
