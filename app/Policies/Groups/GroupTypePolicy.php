<?php

namespace App\Policies\Groups;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class GroupTypePolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_type_create'))
            $can = true;

        if( ! $can )
            $this->_log('group_type_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_type_update'))
            $can = true;

        if( ! $can )
            $this->_log('group_type_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_type_delete'))
            $can = true;

        if( ! $can )
            $this->_log('group_type_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_type_show'))
            $can = true;

        if( ! $can )
            $this->_log('group_type_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('group_type_search'))
            $can = true;

        if( ! $can )
            $this->_log('group_type_search', 'User', $user);

        return $can;
    }
}
