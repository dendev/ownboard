<?php

namespace App\Policies\Members;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class MemberAnnouncementPolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function member_announcement_get_groups(User $user ): bool
    {
        $can = false;

        if( $user->hasPermissionTo('member_announcement_get_groups'))
            $can = true;

        if( ! $can )
            $this->_log('member_announcement_get_groups', 'User', $user);

        return $can;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('member_user_create'))
            $can = true;

        if( ! $can )
            $this->_log('member_user_create', 'User', $user);

        return $can;
    }

    public function delete(User $user)
    {
         $can = false;

        if( $user->hasPermissionTo('member_user_delete'))
            $can = true;

        if( ! $can )
            $this->_log('member_user_delete', 'User', $user);

        return $can;
    }

    public function search(User $user)
    {
         $can = false;

        if( $user->hasPermissionTo('member_user_search'))
            $can = true;

        if( ! $can )
            $this->_log('member_user_search', 'User', $user);

        return $can;
    }

    public function update(User $user)
    {
         $can = false;

        if( $user->hasPermissionTo('member_user_update'))
            $can = true;

        if( ! $can )
            $this->_log('member_user_update', 'User', $user);

        return $can;
    }
}
