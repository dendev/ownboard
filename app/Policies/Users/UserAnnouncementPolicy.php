<?php

namespace App\Policies\Users;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class UserAnnouncementPolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }


    public function user_get_announcements(User $user, User $model)
    {
         $can = false;

        if( $user->hasPermissionTo('user_get_announcements'))
            $can = true;

        if( ! $can )
            $this->_log('user_get_announcements', 'User', $user);

        return $can;
    }
}
