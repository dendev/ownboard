<?php

namespace App\Policies\Users;

use App\Models\Users\User;
use App\Traits\UtilPolicy;

class UserPolicy
{
    use UtilPolicy;

    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    public function create(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('user_create'))
            $can = true;

        if( ! $can )
            $this->_log('user_create', 'User', $user);

        return $can;
    }

    public function update(User $user): bool // TODO update specific user
    {
        $can = false;

        if( $user->hasPermissionTo('user_update'))
            $can = true;

        if( ! $can )
            $this->_log('user_update', 'User', $user);

        return $can;
    }

    public function delete(User $user): bool // TODO delete specific user
    {
        $can = false;

        if( $user->hasPermissionTo('user_delete'))
            $can = true;

        if( ! $can )
            $this->_log('user_delete', 'User', $user);

        return $can;
    }

    public function show(User $user): bool // TODO show specific user
    {
        $can = false;

        if( $user->hasPermissionTo('user_show'))
            $can = true;

        if( ! $can )
            $this->_log('user_show', 'User', $user);

        return $can;
    }

    public function search(User $user): bool
    {
        $can = false;

        if( $user->hasPermissionTo('user_search'))
            $can = true;

        if( ! $can )
            $this->_log('user_search', 'User', $user);

        return $can;
    }

    public function get_announcements(User $user, User $model)
    {
         $can = false;

        if( $user->hasPermissionTo('user_get_announcements'))
            $can = true;

        if( ! $can )
            $this->_log('user_get_announcements', 'User', $user);

        return $can;
    }
}
