<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementCategory;
use App\Models\Announcements\AnnouncementHistoricStatus;
use App\Models\Announcements\AnnouncementImage;
use App\Models\Announcements\AnnouncementLink;
use App\Models\Announcements\AnnouncementListLink;
use App\Models\Announcements\AnnouncementPriority;
use App\Models\Announcements\AnnouncementStatus;
use App\Models\Announcements\AnnouncementType;
use App\Models\Groups\Group;
use App\Models\Groups\GroupCategory;
use App\Models\Groups\GroupMember;
use App\Models\Groups\GroupType;
use App\Models\Members\MemberAnnouncement;
use App\Models\Members\MemberUser;
use App\Models\Users\User;
use App\Models\Users\UserAnnouncement;
use App\Policies\Announcements\AnnouncementCategoryPolicy;
use App\Policies\Announcements\AnnouncementHistoricStatusPolicy;
use App\Policies\Announcements\AnnouncementImagePolicy;
use App\Policies\Announcements\AnnouncementLinkPolicy;
use App\Policies\Announcements\AnnouncementListLinkPolicy;
use App\Policies\Announcements\AnnouncementPolicy;
use App\Policies\Announcements\AnnouncementPriorityPolicy;
use App\Policies\Announcements\AnnouncementStatusPolicy;
use App\Policies\Announcements\AnnouncementTypePolicy;
use App\Policies\Groups\GroupCategoryPolicy;
use App\Policies\Groups\GroupMemberPolicy;
use App\Policies\Groups\GroupPolicy;
use App\Policies\Groups\GroupTypePolicy;
use App\Policies\Members\MemberAnnouncementPolicy;
use App\Policies\Members\MemberUserPolicy;
use App\Policies\Users\UserAnnouncementPolicy;
use App\Policies\Users\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = array(
        // users
        User::class => UserPolicy::class,
        UserAnnouncement::class => UserAnnouncementPolicy::class,

        // groups
        GroupMember::class => GroupMemberPolicy::class,
        GroupCategory::class => GroupCategoryPolicy::class,
        GroupType::class => GroupTypePolicy::class,
        Group::class => GroupPolicy::class,

        // members
        MemberUser::class => MemberUserPolicy::class,
        MemberAnnouncement::class => MemberAnnouncementPolicy::class,

        // announcements
        AnnouncementCategory::class => AnnouncementCategoryPolicy::class,
        AnnouncementStatus::class => AnnouncementStatusPolicy::class,
        AnnouncementHistoricStatus::class => AnnouncementHistoricStatusPolicy::class,
        AnnouncementLink::class => AnnouncementLinkPolicy::class,
        AnnouncementListLink::class => AnnouncementListLinkPolicy::class,
        AnnouncementType::class => AnnouncementTypePolicy::class,
        AnnouncementPriority::class => AnnouncementPriorityPolicy::class,
        AnnouncementImage::class => AnnouncementimagePolicy::class,
        Announcement::class => AnnouncementPolicy::class,
    );

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}
