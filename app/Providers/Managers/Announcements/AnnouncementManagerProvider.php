<?php

namespace App\Providers\Managers\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Managers\Announcements\AnnouncementManagerService;

class AnnouncementManagerProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_manager', function ($app) {
            return new AnnouncementManagerService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
