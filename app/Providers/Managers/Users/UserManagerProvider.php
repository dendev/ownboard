<?php

namespace App\Providers\Managers\Users;

use Illuminate\Support\ServiceProvider;
use App\Services\Managers\Users\UserManagerService;

class UserManagerProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'user_manager', function ($app) {
            return new UserManagerService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
