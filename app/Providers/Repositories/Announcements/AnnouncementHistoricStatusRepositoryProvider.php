<?php

namespace App\Providers\Repositories\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Announcements\AnnouncementHistoricStatusRepositoryService;

class AnnouncementHistoricStatusRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_historic_status_repository', function ($app) {
            return new AnnouncementHistoricStatusRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
