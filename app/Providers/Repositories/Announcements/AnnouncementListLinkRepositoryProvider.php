<?php

namespace App\Providers\Repositories\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Announcements\AnnouncementListLinkRepositoryService;

class AnnouncementListLinkRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_list_link_repository', function ($app) {
            return new AnnouncementListLinkRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
