<?php

namespace App\Providers\Repositories\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Announcements\AnnouncementPriorityRepositoryService;

class AnnouncementPriorityRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_priority_repository', function ($app) {
            return new AnnouncementPriorityRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
