<?php

namespace App\Providers\Repositories\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Announcements\AnnouncementRepositoryService;

class AnnouncementRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_repository', function ($app) {
            return new AnnouncementRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
