<?php

namespace App\Providers\Repositories\Announcements;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Announcements\AnnouncementStatusRepositoryService;

class AnnouncementStatusRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'announcement_status_repository', function ($app) {
            return new AnnouncementStatusRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
