<?php

namespace App\Providers\Repositories\Groups;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Groups\GroupCategoryRepositoryService;

class GroupCategoryRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'group_category_repository', function ($app) {
            return new GroupCategoryRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
