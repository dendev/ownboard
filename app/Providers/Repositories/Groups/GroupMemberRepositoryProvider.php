<?php

namespace App\Providers\Repositories\Groups;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Groups\GroupMemberRepositoryService;

class GroupMemberRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'group_member_repository', function ($app) {
            return new GroupMemberRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
