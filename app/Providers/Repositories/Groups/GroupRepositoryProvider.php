<?php

namespace App\Providers\Repositories\Groups;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Groups\GroupRepositoryService;

class GroupRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'group_repository', function ($app) {
            return new GroupRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
