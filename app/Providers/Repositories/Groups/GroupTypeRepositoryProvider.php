<?php

namespace App\Providers\Repositories\Groups;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Groups\GroupTypeRepositoryService;

class GroupTypeRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'group_type_repository', function ($app) {
            return new GroupTypeRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
