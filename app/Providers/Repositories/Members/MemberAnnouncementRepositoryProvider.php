<?php

namespace App\Providers\Repositories\Members;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Members\MemberAnnouncementRepositoryService;

class MemberAnnouncementRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'member_announcement_repository', function ($app) {
            return new MemberAnnouncementRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
