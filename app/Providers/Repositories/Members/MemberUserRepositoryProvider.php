<?php

namespace App\Providers\Repositories\Members;

use App\Services\Repositories\Members\MemberUserRepositoryService;
use Illuminate\Support\ServiceProvider;

class MemberUserRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'member_user_repository', function ($app) {
            return new MemberUserRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
