<?php

namespace App\Providers\Repositories\Users;

use App\Services\Repositories\Users\UserRepositoryService;
use Illuminate\Support\ServiceProvider;

class UserRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
       $this->app->singleton( 'user_repository', function ($app) {
            return new UserRepositoryService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
