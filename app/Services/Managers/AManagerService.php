<?php

namespace App\Services\Managers;

use App\Models\Users\User;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;

abstract class AManagerService implements IManagerService
{
    use UtilService;

    public function check_access(int|string|User $id_or_model_actor, string $action, ?Model $model = null): bool
    {
        $can = false;

        $actor = $this->_instantiate_if_id($id_or_model_actor, User::class);
        if( $actor )
        {
            if( is_null($model) )
                $can = $actor->can($action, $this->_model_class);
            else
                $can = $actor->can($action, $model);

            if( $can )
            {
                $can = true;
                \Log::stack(['manager', 'stack'])->debug("[ARepositoryService::log_access] ARS:la01 : user ( {$actor->email} {$actor->id}) can do '$action' to {$this->_model_class}", [
                    'user_doing_id' => $actor->id,
                    'user_doing_email' => $actor->email,
                    'model' => $this->_model_class,
                ]);
            }
            else
            {
                \Log::stack(['manager', 'stack'])->warning("[ARepositoryService::log_access] ARS:la01 : user ( {$actor->email} {$actor->id}) can't do '$action' to {$this->_model_class}", [
                    'user_doing_id' => $actor->id,
                    'user_doing_email' => $actor->email,
                    'model' => $this->_model_class,
                ]);
            }
        }
        else
        {
            \Log::stack(['manager', 'stack'])->error("[ARepositoryService::log_access] actor can be found", [
                'id_or_model_actor' => $id_or_model_actor,
                'actor_id' => $actor->id,
                'actor_email' => $actor->email,
            ]);
        }


        return $can;
    }

}
