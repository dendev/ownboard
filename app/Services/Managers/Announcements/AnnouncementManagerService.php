<?php
namespace App\Services\Managers\Announcements;

use App\Models\Members\MemberAnnouncement;
use App\Models\Users\User;
use App\Services\Managers\AManagerService;
use App\Services\Repositories\ARepositoryService;
use App\Traits\UtilService;
use App\Models\Announcements\Announcement;
use Illuminate\Support\Collection;

class AnnouncementManagerService extends AManagerService
{
    use UtilService;

    protected string $_model_class = User::class;

    public function test_me(): string
    {
        return 'announcement_manager';
    }

    public function get_groups($id_or_model_announcement, $id_or_model_user_doing): Collection
    {
        $groups = collect();

        $announcement = $this->_instantiate_if_id($id_or_model_announcement, Announcement::class);
        $user_doing = $this->_instantiate_if_id($id_or_model_user_doing, User::class);

        if( $announcement && $user_doing )
        {
            $this->_model_class = MemberAnnouncement::class; // allow to use MemberUserPolicy

            if( $this->check_access($user_doing, 'member_announcement_get_groups') )
            {
                $groups = \MemberAnnouncementRepository::get_groups($announcement, $user_doing);
            }
            else
            {
                \Log::stack(['manager', 'stack'])->error("[AnnouncementManagerService::get_groups] AMSgg01 : User without 'member_announcement_get_groups' permission try to get groups of annoucements",[
                    'announcement_id' => $announcement->id,
                    'announcement_label' => $announcement->label,
                    'user_doing_id' => $user_doing->id,
                    'user_doing_email' => $user_doing->email,
                    'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
                ]);
            }

            $this->_model_class = Announcement::class; // default
        }
        else
        {
            \Log::stack(['manager', 'stack'])->error("[AnnouncementManagerService::get_groups] AMSgg01 : User without 'member_get_groups' permission try to get groups of announcement",[
                'announcement_id' => $announcement->id,
                'announcement_label' => $announcement->label,
                'user_doing_id' => $user_doing->id,
                'user_doing_email' => $user_doing->email,
                'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
            ]);
        }


        return $groups;
    }
}
