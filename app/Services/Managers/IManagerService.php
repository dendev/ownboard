<?php

namespace App\Services\Managers;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

interface IManagerService
{
    public function check_access(int|string|User $id_or_model_actor, string $action, ?Model $model = null): bool;
}
