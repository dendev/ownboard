<?php
namespace App\Services\Managers\Users;

use App\Models\Members\MemberUser;
use App\Models\Users\User;
use App\Models\Users\UserAnnouncement;
use App\Services\Managers\AManagerService;
use App\Traits\UtilService;
use Illuminate\Support\Collection;

class UserManagerService extends AManagerService
{
    use UtilService;

    protected string $_model_class = User::class;

    public function test_me(): string
    {
        return 'user_manager';
    }

    public function get_groups($id_or_model_user, $id_or_model_user_doing): Collection
    {
        $groups = collect();

        $user = $this->_instantiate_if_id($id_or_model_user, User::class);
        $user_doing = $this->_instantiate_if_id($id_or_model_user_doing, User::class);

        if( $user && $user_doing )
        {
            $this->_model_class = MemberUser::class; // allow to use MemberUserPolicy

            if( $this->check_access($user_doing, 'member_user_get_groups') )
            {
                $groups = \MemberUserRepository::get_groups($user, $user_doing);
            }
            else
            {
                \Log::stack(['manager', 'stack'])->error("[UserManagerService::get_groups] UMSgg02 : User without 'member_get_groups' permission try to get groups",[
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_doing_id' => $user_doing->id,
                    'user_doing_email' => $user_doing->email,
                    'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
                ]);
            }

            $this->_model_class = User::class; // default
        }
        else
        {
            \Log::stack(['manager', 'stack'])->error("[UserManagerService::get_groups] UMSgg01 : Missing user or user doing",[
                'user' => $id_or_model_user,
                'user_doing' => $id_or_model_user_doing,
            ]);
        }


        return $groups;
    }

    public function get_announcements_for_user(mixed $id_or_model_user, mixed $id_or_model_user_doing): Collection
    {
        return $this->_get_announcements_for_user($id_or_model_user, $id_or_model_user_doing);
    }

    public function get_current_announcements_for_user(mixed $id_or_model_user, mixed $id_or_model_user_doing, false|\DateTime $date = false ): Collection
    {
        return $this->_get_announcements_for_user($id_or_model_user, $id_or_model_user_doing,
            null, null, null,
            true);
    }

    public function get_current_published_announcements_for_user(mixed $id_or_model_user, mixed $id_or_model_user_doing, false|\DateTime $date = false ): Collection
    {
        return $this->_get_announcements_for_user($id_or_model_user, $id_or_model_user_doing,
            null, null, 'published', true );
    }

    private function _get_announcements_for_user(mixed $id_or_model_user, mixed $id_or_model_user_doing,
                                                 ?string $category_identity = null, ?string $type_identity = null, ?string $status_identity = null,
                                                 ?bool $only_current = null,
                                                 ?bool $can_view = null, ?bool $can_remove = null, ?bool $can_add = null,
                                                 mixed $where_id_or_model_user_creator = null, mixed $where_id_or_model_user_corrector = null,
                                                 ?\DateTime $fake_date = null ): Collection
    {
        $announcements = collect();

        $date = ! is_null($fake_date ) ? $fake_date : now();

        $user = $this->_instantiate_if_id($id_or_model_user, User::class);
        $user_doing = $this->_instantiate_if_id($id_or_model_user_doing, User::class);

        if( $user && $user_doing )
        {
            $this->_model_class = UserAnnouncement::class; // allow to use MemberUserPolicy

            if( $this->check_access($user_doing, 'user_get_announcements') )
            {
                $rq = UserAnnouncement::where('user_id', $user->id)
                    ->distinct('announcement_id');

                // classifications
                if( ! is_null( $category_identity) )
                    $rq->where('category_identity', '=', $category_identity);

                if( ! is_null( $type_identity) )
                    $rq->where('type_identity', '=', $type_identity);

                if( ! is_null( $status_identity) )
                    $rq->where('status_identity', '=', $status_identity);

                // current
                if( $only_current )
                    $rq->where('begin_at', '<=', $date)
                    ->where('end_at', '>=', $date);

                // perms
                if( ! is_null($can_view ))
                    $rq->where('user_can_view_announcements', $can_view);

                if( ! is_null($can_remove ))
                    $rq->where('user_can_remove_announcements', $can_remove);

                if( ! is_null($can_add ))
                    $rq->where('user_can_add_announcements', $can_add);

                // users
                if( ! is_null( $where_id_or_model_user_creator) )
                {
                    $creator = $this->_instantiate_if_id($where_id_or_model_user_creator, User::class);
                    $rq->where('creator_id', $creator->id);
                }

                if( ! is_null( $where_id_or_model_user_corrector) )
                {
                    $corrector = $this->_instantiate_if_id($where_id_or_model_user_corrector, User::class);
                    $rq->where('corrector_id', $corrector->id);
                }

                // debug
                //dump( $user->id );
                //dump( $type_identity );
                //dd( $rq->toSql());

                $announcements = $rq->get();
            }
            else
            {
                \Log::stack(['manager', 'stack'])->error("[UserManagerService::get_announcements] UMSga02 : User without 'user_get_announcements' permission try to get announcements for user",[
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_doing_id' => $user_doing->id,
                    'user_doing_email' => $user_doing->email,
                    'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
                ]);
            }

            $this->_model_class = User::class; // default
        }
        else
        {
            \Log::stack(['manager', 'stack'])->error("[UserManagerService::get_current_announcements_for_user] UMSgcafu01 : User without 'member_get_groups' permission try to get groups",[
                'user' => $id_or_model_user,
                'user_doing' => $id_or_model_user_doing,
            ]);
        }

        return $announcements;
    }
}
