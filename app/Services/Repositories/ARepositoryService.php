<?php

namespace App\Services\Repositories;

use App\Models\Users\User;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class ARepositoryService implements IRepositoryService
{
    use UtilService;

    public function check_access(int|string|User $id_or_model_actor, string $action, ?Model $model = null): bool
    {
        $can = false;

        $actor = $this->_instantiate_if_id($id_or_model_actor, User::class);
        if( $actor )
        {
            if( is_null($model) )
                $can = $actor->can($action, $this->_model_class);
            else
                $can = $actor->can($action, $model);

            if( $can )
            {
                $can = true;
                \Log::stack(['repository', 'stack'])->debug("[ARepositoryService::log_access] ARS:la01 : user ( {$actor->email} {$actor->id}) can do '$action' to {$this->_model_class}", []);
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::log_access] ARS:la01 : user ( {$actor->email} {$actor->id}) can't do '$action' to {$this->_model_class}", []);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->error("[ARepositoryService::log_access] actor can be found", [
                'id_or_model_actor' => $id_or_model_actor,
                'actor_id' => $actor->id,
                'actor_email' => $actor->email,
            ]);
        }


        return $can;
    }

    public function create(array $datas, int|string|User $id_or_model_actor): false|Model
    {
        $model = false;

        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class); // synonym user_doing && actor
        if( $user_doing )
        {
            if( $this->check_access($user_doing, 'create'))
            {
                $model = new $this->_model_class($datas);
                $model->save();
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::create] ARS:c02 : user ( {$user_doing->email} {$user_doing->id}) can't do 'create' to {$this->_model_class}", []);
            }
        }
        else
        {
                 \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::create] ARS:c01 : user not found so we can't check his permissions", [
                     'user_id' => $id_or_model_actor
                 ]);
        }

        return $model;
    }

    public function delete(int|string|Model $id_or_model, int|string|User $id_or_model_actor): bool|Model
    {
        $model = false;

        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class); // synonym user_doing && actor
        if( $user_doing )
        {
            if ($this->check_access($id_or_model_actor, 'delete'))
            {
                $model = $this->_instantiate_if_id($id_or_model, $this->_model_class);
                $model->delete();
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::delete] ARS:d02 : user ( {$user_doing->email} {$user_doing->id}) can't do 'create' to {$this->_model_class}", []);
            }
        }
        else
        {
                 \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::delete] ARS:d01 : user not found so we can't check his permissions", [
                     'user_id' => $id_or_model_actor
                 ]);
        }

        return $model;
    }

    public function find(int|string $id, int|string|User $id_or_model_actor): false|Model
    {
        $found = false;
        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class); // synonym user_doing && actor
        if( $user_doing )
        {
            if ($this->check_access($id_or_model_actor, 'search'))
            {
                $found = $this->_model_class::find($id);
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find] ARS:f02 user ( {$user_doing->email} {$user_doing->id}) can't do 'find' to {$this->_model_class}", []);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find] ARS:f01 : user not found so we can't check his permissions", [
                'user_id' => $id_or_model_actor
            ]);
        }


        return $found;
    }

    public function find_by_identity(string $identity, int|string|User $id_or_model_actor): false|Model
    {
        $found = false;

        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class);
        if( $user_doing )
        {
            if( $this->check_access($id_or_model_actor, 'search'))
            {
                $found = $this->_model_class::where('identity', $identity)->first();
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find_by_identity] ARS:fbi02 user ( {$user_doing->email} {$user_doing->id}) can't do 'search' to {$this->_model_class}", []);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find_by_identity] ARS:fbi01 : user not found so we can't check his permissions", [
                'user_id' => $id_or_model_actor
            ]);
        }

        return $found;
    }

    public function get(int|string $id, int|string|User $id_or_model_actor): false|Model
    {
        return $this->find($id, $id_or_model_actor);
    }

    public function get_by_identity(int|string $identity, int|string|User $id_or_model_actor): false|Model
    {
        return $this->find_by_identity($identity, $id_or_model_actor);
    }

    public function get_all(int|string|User $id_or_model_actor, ?array $where = []): false|Collection
    {
        $models = false;

        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class);

        if( $user_doing )
        {
            if( $this->check_access($id_or_model_actor, 'search')) // TODO bof create list ?
            {
                $models = $this->_model_class::where($where)->get();
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::get_all] ARS:fbi02 user ( {$user_doing->email} {$user_doing->id}) can't do 'search' to {$this->_model_class}", []);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::get_all] ARS:fbi01 : user not found so we can't check his permissions", [
                'user_id' => $id_or_model_actor
            ]);
        }

        return $models;
    }

    public function update(mixed $id_or_model, array $datas, int|string|User $id_or_model_actor): bool|Model
    {
        $model = false;

        $user_doing = $this->_instantiate_if_id($id_or_model_actor, User::class);

        if( $user_doing )
        {
            if( $this->check_access($id_or_model_actor, 'update'))
            {
                $model = $this->_instantiate_if_id($id_or_model, $this->_model_class);
                $model->update($datas);
            }
            else
            {
                \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::update] ARS:u02 user ( {$user_doing->email} {$user_doing->id}) can't do 'update' to {$this->_model_class}", []);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->warning("[ARepositoryService::update] ARS:u01 : user not found so we can't check his permissions", [
                'user_id' => $id_or_model_actor
            ]);
        }

        return $model;
    }
}
