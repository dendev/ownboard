<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementCategory;

class AnnouncementCategoryRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementCategory::class;

    public function test_me(): string
    {
        return 'announcement_category_repository';
    }
}
