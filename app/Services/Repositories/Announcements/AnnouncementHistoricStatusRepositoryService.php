<?php
namespace App\Services\Repositories\Announcements;

use App\Models\Users\User;
use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementHistoricStatus;
use Illuminate\Database\Eloquent\Model;

class AnnouncementHistoricStatusRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementHistoricStatus::class;

    public function test_me(): string
    {
        return 'announcement_historic_status_repository';
    }

    public function find_by_identity(string $identity, User|int|string $id_or_model_actor): false|Model
    {
        return false; // FIXME error or false ? // no identity exist for historic status
    }
}
