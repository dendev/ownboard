<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementImage;

class AnnouncementImageRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementImage::class;

    public function test_me(): string
    {
        return 'announcement_image_repository';
    }
}
