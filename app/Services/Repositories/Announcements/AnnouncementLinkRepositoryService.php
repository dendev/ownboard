<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementLink;

class AnnouncementLinkRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementLink::class;

    public function test_me(): string
    {
        return 'announcement_link_repository';
    }
}
