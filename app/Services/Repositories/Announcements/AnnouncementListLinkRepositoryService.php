<?php
namespace App\Services\Repositories\Announcements;

use App\Models\Users\User;
use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementListLink;
use Illuminate\Database\Eloquent\Model;

class AnnouncementListLinkRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementListLink::class;

    public function test_me(): string
    {
        return 'announcement_list_link_repository';
    }

    public function find_by_identity(string $identity, User|int|string $id_or_model_actor): false|Model
    {
        return false;
    }
}
