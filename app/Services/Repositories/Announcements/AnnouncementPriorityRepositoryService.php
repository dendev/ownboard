<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementPriority;

class AnnouncementPriorityRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementPriority::class;

    public function test_me(): string
    {
        return 'announcement_priority_repository';
    }
}
