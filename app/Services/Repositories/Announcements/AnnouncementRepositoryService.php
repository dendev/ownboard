<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\Announcement;

class AnnouncementRepositoryService extends ARepositoryService
{
    protected string $_model_class = Announcement::class;

    public function test_me(): string
    {
        return 'announcement_repository';
    }
}
