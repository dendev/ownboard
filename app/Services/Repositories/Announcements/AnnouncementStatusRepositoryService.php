<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementStatus;

class AnnouncementStatusRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementStatus::class;

    public function test_me(): string
    {
        return 'announcement_status_repository';
    }
}
