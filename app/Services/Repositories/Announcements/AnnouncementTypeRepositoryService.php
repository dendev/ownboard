<?php
namespace App\Services\Repositories\Announcements;

use App\Services\Repositories\ARepositoryService;
use App\Models\Announcements\AnnouncementType;

class AnnouncementTypeRepositoryService extends ARepositoryService
{
    protected string $_model_class = AnnouncementType::class;

    public function test_me(): string
    {
        return 'announcement_type_repository';
    }
}
