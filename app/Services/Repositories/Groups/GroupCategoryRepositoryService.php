<?php
namespace App\Services\Repositories\Groups;

use App\Models\Groups\GroupCategory;
use App\Services\Repositories\ARepositoryService;

class GroupCategoryRepositoryService extends ARepositoryService
{
    protected string $_model_class = GroupCategory::class;

    public function test_me(): string
    {
        return 'group_category_repository';
    }
}
