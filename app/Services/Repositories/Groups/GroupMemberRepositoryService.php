<?php
namespace App\Services\Repositories\Groups;

use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use App\Services\Repositories\ARepositoryService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class GroupMemberRepositoryService extends ARepositoryService
{
    protected string $_model_class = GroupMember::class;

    public function test_me(): string
    {
        return 'group_member_repository';
    }

    public function find_by_identity(string $identity, User|int|string $id_or_model_actor): false|Model
    {
        return false; // unavailable because group member doesn't have identity
    }
}
