<?php
namespace App\Services\Repositories\Groups;

use App\Services\Repositories\ARepositoryService;
use App\Models\Groups\Group;

class GroupRepositoryService extends ARepositoryService
{
    protected string $_model_class = Group::class;

    public function test_me(): string
    {
        return 'group_repository';
    }
}
