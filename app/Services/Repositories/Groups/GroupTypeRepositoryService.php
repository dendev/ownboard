<?php
namespace App\Services\Repositories\Groups;

use App\Models\Groups\GroupType;
use App\Services\Repositories\ARepositoryService;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class GroupTypeRepositoryService extends ARepositoryService
{
    use DatabaseMigrations;

    protected string $_model_class = GroupType::class;

    public function test_me(): string
    {
        return 'group_type_repository';
    }
}
