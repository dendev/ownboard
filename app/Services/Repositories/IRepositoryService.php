<?php

namespace App\Services\Repositories;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface IRepositoryService
{
    public function check_access(int|string|User $id_or_model_actor, string $action, ?Model $model = null): bool;

    public function create(array $datas, int|string|User $id_or_model_actor): false|Model;

    public function delete(int|string|Model $id_or_model, int|string|User $id_or_model_actor): bool|Model;

    public function find(int|string $id, int|string|User $id_or_model_actor): false|Model;

    public function find_by_identity(string $identity, int|string|User $id_or_model_actor): false|Model;

    public function get(int|string $id, int|string|User $id_or_model_actor): false|Model;

    public function get_by_identity(string $identity, int|string|User $id_or_model_actor): false|Model;

    public function get_all(int|string|User $id_or_model_actor, ?array $where = []): false|Collection;

    public function update(mixed $id_or_model, array $datas, int|string|User $id_or_model_actor): bool|Model;
}
