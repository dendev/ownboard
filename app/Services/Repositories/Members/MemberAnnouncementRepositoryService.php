<?php
namespace App\Services\Repositories\Members;

use App\Models\Announcements\Announcement;
use App\Models\Groups\Group;
use App\Models\Users\User;
use App\Services\Repositories\ARepositoryService;
use App\Models\Members\MemberAnnouncement;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MemberAnnouncementRepositoryService extends ARepositoryService
{
    use UtilService;

    protected string $_model_class = MemberAnnouncement::class;

    public function test_me(): string
    {
        return 'member_announcement_repository';
    }

    public function create(array $datas, User|int|string $id_or_model_actor): false|Model
    {

        $datas = $this->_child_to_parent($datas);

        return parent::create($datas, $id_or_model_actor);
    }

    public function update(mixed $id_or_model, array $datas, User|int|string $id_or_model_actor): bool|Model
    {
        $datas = $this->_child_to_parent($datas);

        return parent::update($id_or_model, $datas, $id_or_model_actor);
    }

    public function get_groups(mixed $id_or_model_announcement, mixed $id_or_model_user_doing): Collection
    {
        $groups = collect();

        $announcement = $this->_instantiate_if_id($id_or_model_announcement, Announcement::class);
        $user_doing = $this->_instantiate_if_id($id_or_model_user_doing, User::class);

        if( $announcement && $user_doing)
        {
            if( $this->check_access($user_doing, 'member_announcement_get_groups') )
            {
                $announcement_id = intval($announcement->id);
                $subquery = "(select * from member_announcements as mu where mu.announcement_id = $announcement_id ) as mu";
                $rq = DB::table('groups', 'g')
                    //->join(DB::raw('(select * from member_users as mu where mu.user_id = 4 ) as mu' ), function($join){ $join->on('g.id', '=', 'mu.group_id');})
                    ->join(DB::raw($subquery), function ($join) {
                        $join->on('g.id', '=', 'mu.group_id');
                    })
                    ->select('g.*');

                $groups = $rq->get();


                if ($groups->isNotEmpty())
                    $groups = Group::soak($groups->toArray(), ['category', 'type']); // TODO fixed or from args ?
            }
            else
            {
                \Log::stack(['security', 'repository', 'stack'])->error("[MemberAnnouncementRepositoryService::get_groups] MARSgg01 : User without 'member_get_groups' permission try to get groups",[
                    'announcement_id' => $announcement->id,
                    'announcement_label' => $announcement->label,
                    'user_doing_id' => $user_doing->id,
                    'user_doing_email' => $user_doing->email,
                    'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
                ]);
            }
        }
        else
        {
            dd('TODO log');
            // TODO log
        }

        return $groups;
    }

    private function _child_to_parent(array $datas): array
    {
        if( array_key_exists('announcement_id', $datas))
        {
            $datas['member_table'] = 'announcements';
            $datas['member_id'] = $datas['announcement_id'];
            unset($datas['announcement_id']);
        }
        // TODO thrown custom explicite error

        return $datas;
    }
}
