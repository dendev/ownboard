<?php
namespace App\Services\Repositories\Members;

use App\Models\Groups\Group;
use App\Models\Users\User;
use App\Models\Members\MemberUser;
use App\Services\Repositories\Groups\GroupMemberRepositoryService;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MemberUserRepositoryService extends GroupMemberRepositoryService
{
    use UtilService;

    protected string $_model_class = MemberUser::class;

    public function test_me(): string
    {
        return 'member_user_repository';
    }

    public function create(array $datas, User|int|string $id_or_model_actor): false|Model
    {

        $datas = $this->_child_to_parent($datas);

        return parent::create($datas, $id_or_model_actor);
    }

    public function update(mixed $id_or_model, array $datas, User|int|string $id_or_model_actor): bool|Model
    {
        $datas = $this->_child_to_parent($datas);

        return parent::update($id_or_model, $datas, $id_or_model_actor);
    }

    public function get_groups(mixed $id_or_model_user, mixed $id_or_model_user_doing): Collection
    {
        $groups = collect();

        $user = $this->_instantiate_if_id($id_or_model_user, User::class);
        $user_doing = $this->_instantiate_if_id($id_or_model_user_doing, User::class);

        if( $user && $user_doing)
        {
            if( $this->check_access($user_doing, 'member_user_get_groups') )
            {
                $user_id = intval($user->id);
                $subquery = "(select * from member_users as mu where mu.user_id = $user_id ) as mu";
                $rq = DB::table('groups', 'g')
                    //->join(DB::raw('(select * from member_users as mu where mu.user_id = 4 ) as mu' ), function($join){ $join->on('g.id', '=', 'mu.group_id');})
                    ->join(DB::raw($subquery), function ($join) {
                        $join->on('g.id', '=', 'mu.group_id');
                    })
                    ->select('g.*');

                $groups = $rq->get();


                if ($groups->isNotEmpty())
                    $groups = Group::soak($groups->toArray(), ['category', 'type']); // TODO fixed or from args ?
            }
            else
            {
                \Log::stack(['security', 'repository', 'stack'])->error("[MemberUserRepositoryService::get_groups] MURSgg01 : User without 'member_get_groups' permission try to get groups",[
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_doing_id' => $user_doing->id,
                    'user_doing_email' => $user_doing->email,
                    'user_doing_permissions' => implode(', ', $user_doing->permissions->toArray() )
                ]);
            }
        }
        else
        {
            \Log::stack(['repository', 'stack'])->warning("MemberUSerRepositoryService::log_access] ARS:la01 : user ( {$user_doing->email} {$user_doing->id}) can't do 'member_user_get_groups' to {$this->_model_class}", []);
        }

        return $groups;
    }

    private function _child_to_parent(array $datas): array
    {
        if( array_key_exists('user_id', $datas))
        {
            $datas['member_table'] = 'users';
            $datas['member_id'] = $datas['user_id'];
            unset($datas['user_id']);
        }
        // TODO thrown custom explicite error

        return $datas;
    }
}
