<?php
namespace App\Services\Repositories\Users;

use App\Models\Users\User;
use App\Services\Repositories\ARepositoryService;

class UserRepositoryService extends ARepositoryService
{
    protected string $_model_class = User::class;

    public function test_me(): string
    {
        return 'user_repository';
    }
}
