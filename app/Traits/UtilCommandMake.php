<?php

namespace App\Traits;

use Illuminate\Support\Pluralizer;

trait UtilCommandMake
{
    private string $_name_postfix;
    private string $_stub_filename;
    private string $_name;
    private string $_classname;
    private string $_namespace;

    private function _execute()
    {
        $this->info("\n** Working");

        // set basic
        $this->_set_name();
        $this->_set_classname();
        $this->_set_namespace();
        $this->_set_output_path();

        /*
        // debug
        dump( $this->_name);
        dump( $this->_classname);
        dump( $this->_output_path);
        dump( $this->_namespace);
        dd('debug');
        */

        // check directory output
        $output_path = $this->_output_path;
        $this->_make_directory(dirname($output_path));

        // get stub
        $stub_path = $this->_get_stub_path($this->_stub_filename);

        // fill stub
        $contents = $this->_get_contents($stub_path);

        $this->info("\n*** Check exist $output_path");
        if (!$this->files->exists($output_path))
        {
            $this->files->put($output_path, $contents);
            $this->info("+++ {$output_path} created\n");
        }
        else
        {
            $this->info("!!! {$output_path} already exists\n");
        }
    }

    private function _get_stub_path(string $stub_filename): string
    {
        $this->info("\n*** Get stub file $stub_filename");
        $stub_path = base_path("stubs/{$stub_filename}.stub");
        $this->info("+++ Get $stub_path");

        return $stub_path;
    }

    private function _get_stub_values(): array // TODO more généric !
    {
        $this->info("**** Fill stub file values");

        $this->_stub_custom_values['name'] = $this->_name;
        $this->_stub_custom_values['classname'] = $this->_classname;
        $this->_stub_custom_values['output_path'] = $this->_output_path;
        $this->_stub_custom_values['namespace'] = $this->_namespace;
        $this->_stub_custom_values['identity'] = \Str::snake($this->_name) . '_' .  \Str::snake($this->_type);
        $this->_stub_custom_values['facade'] = '\\' . ucfirst($this->_name) . ucfirst($this->_type);

        $values_as_string = '';
        foreach( $this->_stub_custom_values as $key => $value )
        {
            $values_as_string .= $key . ': ' . $value . PHP_EOL;
        }

        $this->info("++++ Values: " . PHP_EOL . $values_as_string);

        return $this->_stub_custom_values;
    }

    private function _get_contents($stub_path): mixed
    {
        $this->info("\n*** Fill stub fill");
        $stub_variables = $this->_get_stub_values();

        $contents = file_get_contents($stub_path);

        foreach ($stub_variables as $search => $replace)
        {
            $contents = str_replace('{{' . $search . '}}', $replace, $contents);
            $contents = str_replace('{{ ' . $search . ' }}', $replace, $contents);
        }
        $this->info("+++ OK filled");

        return $contents;
    }


    private function _make_directory($path): string
    {
        $this->info("\n*** Check directory ${path}'");
        if (!$this->files->isDirectory($path))
        {
            $this->info("+++ ${path} created");
            $this->files->makeDirectory($path, 0777, true, true);
        }
        {
            $this->info("!!! ${path} already exist");
        }

        return $path;
    }

    private function _set_name(): void
    {
        $tmp = $this->_explode_name();

        $name = end($tmp);
        $this->_name = ucwords(Pluralizer::singular($name));
    }

    private function _set_classname(): void
    {
        $this->_classname = ucwords( $this->_name . $this->_name_postfix);
    }

    private function _set_namespace(): void
    {

        $tmp = $this->_explode_name();
        array_pop($tmp);

        $namespace = implode('/', $tmp);

        $full_namespace = $this->_output_path . $namespace;

        $full_namespace = str_replace('/', '\\', $full_namespace);
        $full_namespace = str_replace('\\\\', '\\', $full_namespace);
        $full_namespace = ucfirst($full_namespace);

        if(str_ends_with($full_namespace, '\\'))
            $full_namespace = substr_replace($full_namespace, '', -1);

        $this->_namespace = $full_namespace;
        /*
        $this->_namespace = '';

        $items = explode('/', $this->_output_path);
        $last_key = array_key_last($items);
        foreach( $items as $key => $item )
            if( $item != '' && $key != $last_key)
                $this->_namespace .= ucfirst($item ) . '\\';


        $this->_namespace = substr($this->_namespace, 0, -1);
        */
    }

    private function _set_output_path(): void
    {
        $output_path = $this->_namespace . '\\' . $this->_classname . '.php';
        $output_path = str_replace('\\', '/', $output_path);
        $output_path = str_replace('//', '/', $output_path);
        $output_path = lcfirst($output_path);
        /*
        $tmp = explode('\\', $this->argument('name'));
        array_shift($tmp);
        array_pop($tmp);
        $sub_path = implode('/', $tmp);
        $output_file_name = $this->_classname . '.php';
        $output_path = $this->_output_path . '/' . $sub_path . '/'. $output_file_name;
        $output_path = str_replace('\\', '/', $output_path);
        $output_path = str_replace('//', '/', $output_path);
        $output_path = str_replace('//', '/', $output_path);

        */
        $this->_output_path = $output_path;
    }

    protected function _explode_name(): array
    {
        $tmp = [];

        if( str_contains($this->argument('name'), '\\' ) )
            $tmp = explode('\\', $this->argument('name'));
        else
            $tmp = explode('/', $this->argument('name'));

        return $tmp;
    }
}
