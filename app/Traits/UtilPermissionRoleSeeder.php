<?php

namespace App\Traits;

trait UtilPermissionRoleSeeder
{
    private function _list_config_files(string $path, array &$configs = []): array
    {
        if( is_dir($path))
        {
            $files = scandir($path);

            foreach ($files as $key => $value)
            {
                $full_path = realpath($path . DIRECTORY_SEPARATOR . $value);

                if (!is_dir($full_path))
                {
                    $configs[] = $full_path;
                }
                else if ($value != "." && $value != "..")
                {
                    $this->_list_config_files($full_path, $configs);
                    if( ! is_dir($full_path))
                        $configs[] = $full_path;
                }
            }

        }
        return $configs;
    }

    private function _config_file_to_config_key(string $app_identity, string $path): string
    {
        $key = false;

        $path = explode($app_identity, $path);
        $path = end( $path );
        $path = str_replace('/', '.', $path);
        $path = str_replace('.php', '', $path);

        if(str_starts_with($path, '.'))
            $path = substr($path, 1, strlen($path));

        $key = $app_identity . '.' . $path;

        return $key;
    }
}
