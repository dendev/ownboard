<?php

namespace App\Traits;

use App\Models\Users\User;

trait UtilPolicy
{
    public function before(User $user, $ability)
    {
        if( $user->hasRole('admin') )
            return true;
    }

    private function _log($action, $type, $user )
    {
        \Log::stack(['stack','security'])->warning("[UtilPolicy::$action] UP00 : User can't do '$action' on '$type'", [
            'user_id' => $user->id,
            'user_name' => $user->name,
            'action' => $action,
            'type' => $type,
        ]);
    }
}
