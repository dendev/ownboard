<?php

return [
    'permissions' => [
        'announcement_category' => [
                'list' => 'announcement_category_list',
                'create' => 'announcement_category_create',
                'update' => 'announcement_category_update',
                'show' => 'announcement_category_show',
                'delete' => 'announcement_category_delete',
            ],
        ],
    'roles' => [
        'announcement_category' => [
            'announcement_category_admin' => ['announcement_category_list', 'announcement_category_create', 'announcement_category_update', 'announcement_category_show', 'announcement_category_delete'],
            'announcement_category_viewer' => ['announcement_category_list', 'announcement_category_show' ],
            'announcement_category_writer' => ['announcement_category_list', 'announcement_category_show', 'announcement_category_create', 'announcement_category_update' ],
        ],
    ]
];
