<?php

return [
    'permissions' => [
        'announcement_historic_status' => [
                'list' => 'announcement_historic_status_list',
                'create' => 'announcement_historic_status_create',
                'update' => 'announcement_historic_status_update',
                'show' => 'announcement_historic_status_show',
                'delete' => 'announcement_historic_status_delete',
            ],
        ],
    'roles' => [
        'announcement_historic_status' => [
            'announcement_historic_status_admin' => ['announcement_historic_status_list', 'announcement_historic_status_create', 'announcement_historic_status_update', 'announcement_historic_status_show', 'announcement_historic_status_delete'],
            'announcement_historic_status_viewer' => ['announcement_historic_status_list', 'announcement_historic_status_show' ],
            'announcement_historic_status_writer' => ['announcement_historic_status_list', 'announcement_historic_status_show', 'announcement_historic_status_create', 'announcement_historic_status_update' ],
        ],
    ]
];
