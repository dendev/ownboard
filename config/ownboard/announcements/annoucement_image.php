<?php

return [
    'permissions' => [
        'announcement_image' => [
                'list' => 'announcement_image_list',
                'create' => 'announcement_image_create',
                'update' => 'announcement_image_update',
                'show' => 'announcement_image_show',
                'delete' => 'announcement_image_delete',
            ],
        ],
    'roles' => [
        'announcement_image' => [
            'announcement_image_admin' => ['announcement_image_list', 'announcement_image_create', 'announcement_image_update', 'announcement_image_show', 'announcement_image_delete'],
            'announcement_image_viewer' => ['announcement_image_list', 'announcement_image_show' ],
            'announcement_image_writer' => ['announcement_image_list', 'announcement_image_show', 'announcement_image_create', 'announcement_image_update' ],
        ],
    ]
];
