<?php

return [
    'permissions' => [
        'announcement_link' => [
                'list' => 'announcement_link_list',
                'create' => 'announcement_link_create',
                'update' => 'announcement_link_update',
                'show' => 'announcement_link_show',
                'delete' => 'announcement_link_delete',
            ],
        ],
    'roles' => [
        'announcement_link' => [
            'announcement_link_admin' => ['announcement_link_list', 'announcement_link_create', 'announcement_link_update', 'announcement_link_show', 'announcement_link_delete'],
            'announcement_link_viewer' => ['announcement_link_list', 'announcement_link_show' ],
            'announcement_link_writer' => ['announcement_link_list', 'announcement_link_show', 'announcement_link_create', 'announcement_link_update' ],
        ],
    ]
];
