<?php

return [
    'permissions' => [
        'announcement_list_link' => [
                'list' => 'announcement_list_link_list',
                'create' => 'announcement_list_link_create',
                'update' => 'announcement_list_link_update',
                'show' => 'announcement_list_link_show',
                'delete' => 'announcement_list_link_delete',
            ],
        ],
    'roles' => [
        'announcement_list_link' => [
            'announcement_list_link_admin' => ['announcement_list_link_list', 'announcement_list_link_create', 'announcement_list_link_update', 'announcement_list_link_show', 'announcement_list_link_delete'],
            'announcement_list_link_viewer' => ['announcement_list_link_list', 'announcement_list_link_show' ],
            'announcement_list_link_writer' => ['announcement_list_link_list', 'announcement_list_link_show', 'announcement_list_link_create', 'announcement_list_link_update' ],
        ],
    ]
];
