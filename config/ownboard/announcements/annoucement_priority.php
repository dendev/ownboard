<?php

return [
    'permissions' => [
        'announcement_priority' => [
                'list' => 'announcement_priority_list',
                'create' => 'announcement_priority_create',
                'update' => 'announcement_priority_update',
                'show' => 'announcement_priority_show',
                'delete' => 'announcement_priority_delete',
            ],
        ],
    'roles' => [
        'announcement_priority' => [
            'announcement_priority_admin' => ['announcement_priority_list', 'announcement_priority_create', 'announcement_priority_update', 'announcement_priority_show', 'announcement_priority_delete'],
            'announcement_priority_viewer' => ['announcement_priority_list', 'announcement_priority_show' ],
            'announcement_priority_writer' => ['announcement_priority_list', 'announcement_priority_show', 'announcement_priority_create', 'announcement_priority_update' ],
        ],
    ]
];
