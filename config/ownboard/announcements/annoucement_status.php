<?php

return [
    'permissions' => [
        'announcement_status' => [
                'list' => 'announcement_status_list',
                'create' => 'announcement_status_create',
                'update' => 'announcement_status_update',
                'show' => 'announcement_status_show',
                'delete' => 'announcement_status_delete',
            ],
        ],
    'roles' => [
        'announcement_status' => [
            'announcement_status_admin' => ['announcement_status_list', 'announcement_status_create', 'announcement_status_update', 'announcement_status_show', 'announcement_status_delete'],
            'announcement_status_viewer' => ['announcement_status_list', 'announcement_status_show' ],
            'announcement_status_writer' => ['announcement_status_list', 'announcement_status_show', 'announcement_status_create', 'announcement_status_update' ],
        ],
    ]
];
