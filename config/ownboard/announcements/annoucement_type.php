<?php

return [
    'permissions' => [
        'announcement_type' => [
                'list' => 'announcement_type_list',
                'create' => 'announcement_type_create',
                'update' => 'announcement_type_update',
                'show' => 'announcement_type_show',
                'delete' => 'announcement_type_delete',
            ],
        ],
    'roles' => [
        'announcement_type' => [
            'announcement_type_admin' => ['announcement_type_list', 'announcement_type_create', 'announcement_type_update', 'announcement_type_show', 'announcement_type_delete'],
            'announcement_type_viewer' => ['announcement_type_list', 'announcement_type_show' ],
            'announcement_type_writer' => ['announcement_type_list', 'announcement_type_show', 'announcement_type_create', 'announcement_type_update' ],
        ],
    ]
];
