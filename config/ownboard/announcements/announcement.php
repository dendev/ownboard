<?php

return [
    'permissions' => [
        'valve' => [
                'list' => 'announcement_list',
                'create' => 'announcement_create',
                'update' => 'announcement_update',
                'show' => 'announcement_show',
                'delete' => 'announcement_delete',
            ],
    ],
    'roles' => [
        'valve' => [
            'announcement_admin' => ['announcement_list', 'announcement_create', 'announcement_update', 'announcement_show', 'announcement_delete'],
            'announcement_viewer' => ['announcement_list', 'announcement_show' ],
            'announcement_writer' => ['announcement_list', 'announcement_show', 'announcement_create', 'announcement_update' ],
        ],
    ]
];
