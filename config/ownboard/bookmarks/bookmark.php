<?php

return [
    'permissions' => [
        'bookmark' => [
                'list' => 'bookmark_list',
                'create' => 'bookmark_create',
                'update' => 'bookmark_update',
                'show' => 'bookmark_show',
                'delete' => 'bookmark_delete',
            ],
    ],
    'roles' => [
        'bookmark' => [
            'bookmark_admin' => ['bookmark_list', 'bookmark_create', 'bookmark_update', 'bookmark_show', 'bookmark_delete'],
            'bookmark_viewer' => ['bookmark_list', 'bookmark_show' ],
            'bookmark_writer' => ['bookmark_list', 'bookmark_show', 'bookmark_create', 'bookmark_update' ],
        ],
    ]
];
