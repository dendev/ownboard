<?php

return [
    'permissions' => [
        'general' => [
            'backend_access' => 'backend_access',
            'setting_manage' => 'setting_manage',
        ]
    ],
    'roles' => [
        'general' => [
            'admin' => [],
        ]
    ]
];
