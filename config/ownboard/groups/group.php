<?php

return [
    'permissions' => [
        'group' => [
            'list' => 'group_list',
            'create' => 'group_create',
            'update' => 'group_update',
            'show' => 'group_show',
            'delete' => 'group_delete',
        ],
    ],
    'roles' => [
        'group' => [
            'group_admin' => ['group_list', 'group_create', 'group_update', 'group_show', 'group_delete'],
            'group_viewer' => ['group_list', 'group_show' ],
            'group_writer' => ['group_list', 'group_show', 'group_create', 'group_update' ],
        ],
    ]
];
