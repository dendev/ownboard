<?php

return [
    'permissions' => [
        'group_category' => [
                'list' => 'group_category_list',
                'create' => 'group_category_create',
                'update' => 'group_category_update',
                'show' => 'group_category_show',
                'delete' => 'group_category_delete',
            ],
        ],
    'roles' => [
        'group_category' => [
            'group_category_admin' => ['group_category_list', 'group_category_create', 'group_category_update', 'group_category_show', 'group_category_delete'],
            'group_category_viewer' => ['group_category_list', 'group_category_show' ],
            'group_category_writer' => ['group_category_list', 'group_category_show', 'group_category_create', 'group_category_update' ],
        ],
    ]
];
