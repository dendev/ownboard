<?php

return [
    'permissions' => [
        'group_member' => [
                'list' => 'group_member_list',
                'create' => 'group_member_create',
                'update' => 'group_member_update',
                'show' => 'group_member_show',
                'delete' => 'group_member_delete',
            ],
        ],
    'roles' => [
        'group_member' => [
            'group_member_admin' => ['group_member_list', 'group_member_create', 'group_member_update', 'group_member_show', 'group_member_delete'],
            'group_member_viewer' => ['group_member_list', 'group_member_show' ],
            'group_member_writer' => ['group_member_list', 'group_member_show', 'group_member_create', 'group_member_update' ],
        ],
    ]
];
