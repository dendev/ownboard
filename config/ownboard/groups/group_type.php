<?php

return [
    'permissions' => [
        'group_type' => [
                'list' => 'group_type_list',
                'create' => 'group_type_create',
                'update' => 'group_type_update',
                'show' => 'group_type_show',
                'delete' => 'group_type_delete',
            ],
    ],
    'roles' => [
        'group_type' => [
            'group_type_admin' => ['group_type_list', 'group_type_create', 'group_type_update', 'group_type_show', 'group_type_delete'],
            'group_type_viewer' => ['group_type_list', 'group_type_show' ],
            'group_type_writer' => ['group_type_list', 'group_type_show', 'group_type_create', 'group_type_update' ],
        ],
    ],
];
