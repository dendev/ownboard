<?php

return [
    'permissions' => [
        'member_announcement' => [
            'list' => 'member_announcement_list',
            'create' => 'member_announcement_create',
            'update' => 'member_announcement_update',
            'show' => 'member_announcement_show',
            'delete' => 'member_announcement_delete',
            'get_groups' => 'member_announcement_get_groups'
        ],
    ],
    'roles' => [
        'member_announcement' => [
            'member_announcement_admin' => ['member_announcement_list', 'member_announcement_create', 'member_announcement_update', 'member_announcement_show', 'member_announcement_delete', 'member_announcement_get_groups'],
            'member_announcement_viewer' => ['member_announcement_list', 'member_announcement_show' ],
            'member_announcement_writer' => ['member_announcement_list', 'member_announcement_show', 'member_announcement_create', 'member_announcement_update' ],
        ],
    ]
];
