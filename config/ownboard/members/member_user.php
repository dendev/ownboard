<?php

return [
    'permissions' => [
        'member_user' => [
            'list' => 'member_user_list',
            'create' => 'member_user_create',
            'update' => 'member_user_update',
            'show' => 'member_user_show',
            'delete' => 'member_user_delete',
            'get_groups' => 'member_user_get_groups'
        ],
    ],
    'roles' => [
        'member_user' => [
            'member_user_admin' => ['member_user_list', 'member_user_create', 'member_user_update', 'member_user_show', 'member_user_delete', 'member_user_get_groups'],
            'member_user_viewer' => ['member_user_list', 'member_user_show' ],
            'member_user_writer' => ['member_user_list', 'member_user_show', 'member_user_create', 'member_user_update' ],
        ],
    ]
];
