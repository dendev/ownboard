<?php

namespace Database\Factories\Announcements;

use App\Models\Announcements\AnnouncementCategory;
use App\Models\Announcements\AnnouncementImage;
use App\Models\Announcements\AnnouncementPriority;
use App\Models\Announcements\AnnouncementStatus;
use App\Models\Announcements\AnnouncementType;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcements\AnnouncementCategory>
 */
class AnnouncementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence;
        $category = AnnouncementCategory::all()->random();
        $type = AnnouncementType::all()->random();
        $status= AnnouncementStatus::all()->random();
        $priority = AnnouncementPriority::all()->random();
        $image = AnnouncementImage::all()->random();
        $user = User::all()->random();
        $creator = $user;
        $corrector = $user;

        return [
            'label' => $label,
            'identity' => Str::snake($label),
            'content' => $this->faker->realText(),
            'category_id' => $category->id,
            'type_id' => $type->id,
            'status_id' => $status->id,
            'priority_id' => $priority->id,
            'begin_at' => $this->faker->dateTime,
            'end_at' => $this->faker->dateTime,
            'image_id' => $image->id,
            'creator_id' => $creator->id,
            'corrector_id' => $corrector->id,
        ];
    }
}
