<?php

namespace Database\Factories\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcements\AnnouncementCategory>
 */
class AnnouncementHistoricStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $announcement = Announcement::all()->random();
        $status = AnnouncementStatus::all()->random();

        return [
            'announcement_id' => $announcement->id,
            'status_id' => $status->id,
        ];
    }
}
