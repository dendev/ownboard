<?php

namespace Database\Factories\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementLink;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcements\AnnouncementCategory>
 */
class AnnouncementListLinkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $announcement = Announcement::all()->random();
        $link = AnnouncementLink::all()->random();

        return [
            'announcement_id' => $announcement->id,
            'link_id' => $link->id,
        ];
    }
}
