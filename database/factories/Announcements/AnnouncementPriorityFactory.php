<?php

namespace Database\Factories\Announcements;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcements\AnnouncementCategory>
 */
class AnnouncementPriorityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {



        return [
            'label' => $this->faker->sentence,
            'identity' => $this->faker->uuid,
            'code' => $this->faker->randomLetter,
            'description' => $this->faker->text,
            'order' => $this->faker->randomLetter,
        ];
    }
}
