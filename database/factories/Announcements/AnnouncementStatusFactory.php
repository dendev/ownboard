<?php

namespace Database\Factories\Announcements;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcements\AnnouncementCategory>
 */
class AnnouncementStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence;

        return [
            'label' => $label,
            'identity' => Str::snake($label),
            'code' => $this->faker->randomLetter(),
            'description' => $this->faker->text,
            'order' => $this->faker->randomLetter,
        ];
    }
}
