<?php

namespace Database\Factories\Groups;

use App\Models\Groups\GroupCategory;
use App\Models\Groups\GroupType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Group>
 */
class GroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence(6, true);
        $category = GroupCategory::all()->random();
        $type = GroupType::all()->random();

        return [
            'label' => $label,
            'identity' => \Str::slug($label),
            'category_id' => $category->id,
            'type_id' => $type->id,
            'description' => $this->faker->text(),
            'order' => $this->faker->randomLetter(),
        ];
    }
}
