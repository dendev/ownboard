<?php

namespace Database\Factories\Groups;

use App\Models\Announcements\Announcement;
use App\Models\Groups\Group;
use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\GroupMember>
 */
class GroupMemberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $group = Group::all()->random();

        $member_refs = [
            'users' => User::class,
            'announcements' => Announcement::class,
            //'bookmarks' => Bookmarks::class,
        ];

        $member_key = $this->faker->randomElement(array_keys($member_refs));
        $member_class = $member_refs[$member_key];

        $member = $member_class::all()->random();

        $is_user = $member_key === 'users';

        $already_exist = GroupMember::where('member_id', $member->id)
            ->where('member_table', $member_key)
            ->where('group_id', $group->id)
            ->get();

        if( $already_exist )
        {
            $group = Group::factory()->create();
        }

        return [
            'member_id' => $member->id,
            'member_table' => $member_key,
            'group_id' => $group->id,
            'is_user' => $is_user,
            'user_can_view_members' => $this->faker->boolean,
            'user_can_remove_member' => $this->faker->boolean,
            'user_can_add_member' => $this->faker->boolean,
            //
        ];
    }
}
