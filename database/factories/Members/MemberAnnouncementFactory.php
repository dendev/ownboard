<?php

namespace Database\Factories\Members;

use App\Models\Announcements\Announcement;
use App\Models\Groups\Group;
use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\GroupMember>
 */
class MemberAnnouncementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $group = Group::all()->random();

        $member_class = Announcement::class;

        $member = $member_class::all()->random();

        $already_exist = GroupMember::where('member_id', $member->id)
            ->where('member_table', 'announcements')
            ->where('group_id', $group->id)
            ->get();

        if( $already_exist )
        {
            $group = Group::factory()->create();
        }

        return [
            'announcement_id' => $member->id,
            'group_id' => $group->id,
            'is_user' => false,
            'user_can_view_members' => false,
            'user_can_remove_member' => false,
            'user_can_add_member' => false,
        ];
    }
}
