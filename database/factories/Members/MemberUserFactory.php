<?php

namespace Database\Factories\Members;

use App\Models\Groups\Group;
use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\GroupMember>
 */
class MemberUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $group = Group::all()->random();

        $member_class = User::class;

        $member = $member_class::all()->random();

        $already_exist = GroupMember::where('member_id', $member->id)
            ->where('member_table', 'users')
            ->where('group_id', $group->id)
            ->get();

        if( $already_exist )
        {
            $group = Group::factory()->create();
        }

        return [
            'user_id' => $member->id,
            'group_id' => $group->id,
            'is_user' => true,
            'user_can_view_members' => $this->faker->boolean,
            'user_can_remove_member' => $this->faker->boolean,
            'user_can_add_member' => $this->faker->boolean,
        ];
    }
}
