<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
        CREATE OR REPLACE VIEW member_users AS
            select gm.id, gm.member_id as user_id, gm.group_id, gm.is_user, gm.user_can_view_members, gm.user_can_remove_member, gm.user_can_add_member, gm.created_at, gm.updated_at
            from group_members gm
            where gm.member_table = 'users';
        ");

        DB::statement("COMMENT ON VIEW member_users IS 'get groups of users'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("DROP VIEW IF EXISTS member_users");
    }
};
