<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('identity');
            $table->text('content');
            $table->foreignId('category_id')->constrained('announcement_categories');
            $table->foreignId('type_id')->constrained('announcement_types');
            $table->foreignId('status_id')->constrained('announcement_status');
            $table->foreignId('priority_id')->constrained('announcement_priorities');
            $table->dateTime('begin_at');
            $table->dateTime('end_at')->nullable();
            $table->foreignId('image_id')->constrained('announcement_images');
            $table->foreignId('creator_id')->constrained('users');
            $table->foreignId('corrector_id')->nullable()->constrained('users');
            $table->foreignId('suppressor_id')->nullable()->constrained('users');
            $table->string('order');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('announcement_historic_status');
        Schema::dropIfExists('announcement_list_links');
        Schema::dropIfExists('announcements');
    }
};
