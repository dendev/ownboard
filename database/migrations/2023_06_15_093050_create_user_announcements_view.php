<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
        CREATE OR REPLACE VIEW user_announcements AS
            select  gmu.user_id, gma.announcement_id, gmu.group_id,
            gmu.is_user, gmu.user_can_view_announcements, gmu.user_can_remove_announcement, gmu.user_can_add_announcement,
            a.category_id, ac.\"identity\" as category_identity, a.type_id, at2.\"identity\" as type_identity, a.status_id, as2.\"identity\" as status_identity, a.begin_at, a.end_at,
            a.creator_id, a.corrector_id, a.suppressor_id,
            gmu.created_at, gmu.updated_at
            from
            (
            select member_id as user_id, group_id, is_user, user_can_view_members as user_can_view_announcements, user_can_remove_member as user_can_remove_announcement, user_can_add_member as user_can_add_announcement, created_at, updated_at
            from group_members gm1
            where member_table = 'users'
            ) as gmu
            inner join (
            select member_id as announcement_id, group_id
            from group_members gm2
            where member_table  = 'announcements'
            ) as gma on gma.group_id = gmu.group_id
            inner join announcements a on gma.announcement_id = a.id
            inner join announcement_status as2 on as2.id = a.status_id
            inner join announcement_categories ac on ac.id  = a.category_id
            inner join announcement_types at2 on at2.id = a.type_id
        ");

        DB::statement("COMMENT ON VIEW user_announcements IS 'get announcements for users'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("DROP VIEW IF EXISTS user_announcements");
    }
};
