<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementCategory;
use App\Models\Groups\GroupCategory;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementCategorySeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_categories'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementCategory::factory(15)->create();
        }
    }
}
