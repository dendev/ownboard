<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementHistoricStatus;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementHistoricStatusSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_historic_status'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementHistoricStatus::factory(15)->create();
        }
    }
}
