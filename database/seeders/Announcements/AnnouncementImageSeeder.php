<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementImage;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementImageSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_images'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementImage::factory(15)->create();
        }
    }
}
