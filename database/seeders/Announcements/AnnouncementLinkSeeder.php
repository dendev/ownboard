<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementLink;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementLinkSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_links'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementLink::factory(15)->create();
        }
    }
}
