<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementListLink;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementListLinkSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_list_links'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementListLink::factory(15)->create();
        }
    }
}
