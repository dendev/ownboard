<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementPriority;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementPrioritySeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_priorities'); // warning adminSeeder !

        $datas= [
            ['label' => 'Normal', 'identity' => 'normal', 'code' => 'A', 'description' => '', 'order' => 'A'],
            ['label' => 'Moyene', 'identity' => 'average', 'code' => 'B', 'description' => '', 'order' => 'B'],
            ['label' => 'Haute', 'identity' => 'high', 'code' => 'C', 'description' => '', 'order' => 'C'],
            ['label' => 'Urgence', 'identity' => 'emergency', 'code' => 'D', 'description' => '', 'order' => 'D'],
            ['label' => 'Bloquage', 'identity' => 'blocking', 'code' => 'E', 'description' => '', 'order' => 'E'],
        ];


        foreach( $datas as $data )
        {
            $priority = new AnnouncementPriority($data);
            $priority->save();
        }
    }
}
