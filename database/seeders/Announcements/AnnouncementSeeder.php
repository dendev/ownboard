<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\Announcement;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcements'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            Announcement::factory(15)->create();
        }
    }
}
