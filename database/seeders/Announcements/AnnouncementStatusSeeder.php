<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementStatus;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementStatusSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_status'); // warning adminSeeder !

        $datas = [
            //['label' => 'Published', 'identity' => '', 'code' => '', 'code' => '', 'description' => '', 'order' =>''],
            ['label' => 'Published', 'identity' => 'published', 'code' => 'P00', 'description' => '', 'order' =>'P'],
        ];

        foreach( $datas as $data)
        {
            $status = new AnnouncementStatus($data);
            $status->save();
        }

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementStatus::factory(15)->create();
        }
    }
}
