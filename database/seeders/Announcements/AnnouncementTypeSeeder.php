<?php
namespace Database\Seeders\Announcements;

use App\Models\Announcements\AnnouncementType;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class AnnouncementTypeSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('announcement_types'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            AnnouncementType::factory(15)->create();
        }
    }
}
