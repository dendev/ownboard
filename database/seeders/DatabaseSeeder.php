<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Database\Seeders\Announcements\AnnouncementSeeder;
use Database\Seeders\Announcements\AnnouncementCategorySeeder;
use Database\Seeders\Announcements\AnnouncementHistoricStatusSeeder;
use Database\Seeders\Announcements\AnnouncementImageSeeder;
use Database\Seeders\Announcements\AnnouncementLinkSeeder;
use Database\Seeders\Announcements\AnnouncementListLinkSeeder;
use Database\Seeders\Announcements\AnnouncementPrioritySeeder;
use Database\Seeders\Announcements\AnnouncementStatusSeeder;
use Database\Seeders\Announcements\AnnouncementTypeSeeder;
use Database\Seeders\Groups\GroupCategorySeeder;
use Database\Seeders\Groups\GroupMemberSeeder;
use Database\Seeders\Groups\GroupSeeder;
use Database\Seeders\Groups\GroupTypeSeeder;
use Database\Seeders\Users\AdminSeeder;
use Database\Seeders\Users\PermissionSeeder;
use Database\Seeders\Users\RoleSeeder;
use Database\Seeders\Users\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AdminSeeder::class);

        $this->call(GroupCategorySeeder::class);
        $this->call(GroupTypeSeeder::class);
        $this->call(GroupSeeder::class);

        $this->call(AnnouncementCategorySeeder::class);
        $this->call(AnnouncementStatusSeeder::class);
        $this->call(AnnouncementLinkSeeder::class);
        $this->call(AnnouncementTypeSeeder::class);
        $this->call(AnnouncementPrioritySeeder::class);
        $this->call(AnnouncementImageSeeder::class);
        $this->call(AnnouncementSeeder::class);
        $this->call(AnnouncementHistoricStatusSeeder::class);
        $this->call(AnnouncementListLinkSeeder::class);

        $this->call(GroupMemberSeeder::class);
    }
}
