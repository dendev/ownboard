<?php
namespace Database\Seeders\Groups;

use App\Models\Groups\GroupCategory;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupCategorySeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('group_categories'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            GroupCategory::factory(15)->create();
        }
    }
}
