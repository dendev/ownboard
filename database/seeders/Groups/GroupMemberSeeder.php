<?php

namespace Database\Seeders\Groups;

use App\Models\Groups\GroupMember;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupMemberSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->_reset_table('group_members'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            GroupMember::factory(25)->create();
        }
    }
}
