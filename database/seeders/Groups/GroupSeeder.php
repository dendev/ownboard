<?php

namespace Database\Seeders\Groups;

use App\Models\Groups\Group;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('groups'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            Group::factory(20)->create();
        }
    }
}
