<?php

namespace Database\Seeders\Groups;

use App\Models\Groups\GroupType;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupTypeSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('group_types'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            GroupType::factory(4)->create();
        }
    }
}
