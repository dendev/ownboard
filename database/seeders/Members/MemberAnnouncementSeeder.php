<?php

namespace Database\Seeders\Members;

use App\Models\Members\MemberAnnouncement;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class MemberAnnouncementSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        MemberAnnouncement::whereNotNull('id')->delete();

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            MemberAnnouncement::factory(20)->create();
        }
    }
}
