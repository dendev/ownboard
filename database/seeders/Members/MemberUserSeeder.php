<?php

namespace Database\Seeders\Members;

use App\Models\Members\MemberUser;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class MemberUserSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run()
    {
         MemberUser::whereNotNull('id')->delete();

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            MemberUser::factory(20)->create();
        }
    }
}
