<?php
namespace Database\Seeders\Users;

use App\Traits\UtilPermissionRoleSeeder;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class PermissionSeeder extends Seeder
{
    use UtilSeeder;
    use UtilPermissionRoleSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('permissions');

        if( class_exists('Spatie\Permission\Models\Permission') )
        {
            $app_identity = env('APP_IDENTITY');

            // clean
            Artisan::call('cache:clear');

            // get all config files
            $files = [];
            $this->_list_config_files('./config/' . $app_identity, $files);

            foreach( $files as $file)
            {
                // get config kye
                $config_key = $this->_config_file_to_config_key($app_identity, $file);

                // get values
                $refs = config($config_key . '.permissions') ?? [];

                // iterate permissions
                foreach( $refs as $ref )
                    foreach( $ref as $access => $permission )
                        \Spatie\Permission\Models\Permission::create(['name' => $permission]);
            }
        }
        else
        {
            \Log::error("[PermissionSeeder:run] GPr01 No package spatie permission found. Install it!", []);
        }
    }
}

/*
fix : Spatie\Permission\Exceptions\PermissionAlreadyExists
-> php artisan cache:clear
*/
