<?php
namespace Database\Seeders\Users;

use App\Models\Users\User;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('users'); // warning adminSeeder !

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            User::factory(15)->create();
        }
    }
}
