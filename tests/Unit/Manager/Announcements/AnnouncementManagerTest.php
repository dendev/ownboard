<?php

namespace Tests\Unit\Manager\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Groups\Group;
use App\Models\Members\MemberAnnouncement;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementManagerTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementManager', 'App\Facades\Managers\Announcements\AnnouncementManagerFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementManager::test_me();
        $this->assertEquals('announcement_manager', $exist);
    }

    public function testGetGroups()
    {
        $this->seed();

        $announcement = Announcement::factory()->create();
        $announcement->refresh();
        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();


        $member_announcement_1 = MemberAnnouncement::factory()->create([
            'announcement_id' => $announcement->id,
            'group_id' => $group_1->id,
        ]);

        $member_announcement_2 = MemberAnnouncement::factory()->create([
            'announcement_id' => $announcement->id,
            'group_id' => $group_2->id,
        ]);

        $groups = \AnnouncementManager::get_groups($announcement, $admin);

        $this->assertInstanceOf(Collection::class, $groups);
        $this->assertEquals(2, $groups->count());

        $group_ids = $groups->pluck('id');
        $this->assertContains($group_1->id, $group_ids);
        $this->assertContains($group_2->id, $group_ids);
    }
}
