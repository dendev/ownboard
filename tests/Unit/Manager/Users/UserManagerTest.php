<?php

namespace Tests\Unit\Manager\Users;

use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementStatus;
use App\Models\Groups\Group;
use App\Models\Members\GroupUser;
use App\Models\Members\MemberAnnouncement;
use App\Models\Members\MemberUser;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class UserManagerTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\UserManager', 'App\Facades\Managers\Users\UserManagerFacade');
     }

    public function test_basic(): void
    {
        $exist = \UserManager::test_me();
        $this->assertEquals('user_manager', $exist);
    }

    public function testGetGroups()
    {
        $this->seed();

        $user = User::factory()->create();
        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $groups = \UserManager::get_groups($user, $admin);

        $this->assertInstanceOf(Collection::class, $groups);
        $this->assertEquals(2, $groups->count());

        $group_ids = $groups->pluck('id');
        $this->assertContains($group_1->id, $group_ids);
        $this->assertContains($group_2->id, $group_ids);
    }

    public function testGetAnnouncementsForUser()
    {
        $this->seed();

        $user = User::factory()->create();
        $announcement = Announcement::factory()->create();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $member_announcement_1 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_1->id]);
        $member_announcement_1->save();

        $member_announcement_2 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_2->id]);
        $member_announcement_2->save();

        $announcements = \UserManager::get_announcements_for_user($user, $admin);

        $this->assertInstanceOf(Collection::class, $announcements);
        $this->assertEquals($announcements->count(), 1); // 1 same announcement from two groups
    }

    public function testGetCurrentAnnouncementsForUser()
    {
        $this->seed();

        $user = User::factory()->create();
        $announcement = Announcement::factory()->create([
            'begin_at' => now()->subDays(3),
            'end_at' => now()->addDays(3),
        ]);

        $announcement_begin_after = Announcement::factory()->create([
            'begin_at' => now()->addMonths(3),
            'end_at' => now()->addMonths(3)->addDays(3),
        ]);

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $member_announcement_1 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_1->id]);
        $member_announcement_1->save();

        $member_announcement_2 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_2->id]);
        $member_announcement_2->save();

        $member_announcement_3 = new MemberAnnouncement(['announcement_id' => $announcement_begin_after->id, 'group_id' => $group_1->id]);
        $member_announcement_3->save();

        $member_announcement_4 = new MemberAnnouncement(['announcement_id' => $announcement_begin_after->id, 'group_id' => $group_2->id]);
        $member_announcement_4->save();

        $announcements = \UserManager::get_current_announcements_for_user($user, $admin);

        $this->assertInstanceOf(Collection::class, $announcements);
        $this->assertEquals($announcements->count(), 1);
    }

      public function testGetCurrentPublishedAnnouncementsForUser()
    {
        $this->seed();

        $user = User::factory()->create();

        $status_published = AnnouncementStatus::where('identity', 'published')->first();
        $announcement = Announcement::factory()->create([
            'begin_at' => now()->subDays(3),
            'end_at' => now()->addDays(3),
            'status_id' => $status_published->id,
        ]);

        $announcement_not_published = Announcement::factory()->create([
            'begin_at' => now()->subDays(3),
            'end_at' => now()->addDays(3),
        ]);

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $member_announcement_1 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_1->id]);
        $member_announcement_1->save();

        $member_announcement_2 = new MemberAnnouncement(['announcement_id' => $announcement->id, 'group_id' => $group_2->id]);
        $member_announcement_2->save();

        $member_announcement_3 = new MemberAnnouncement(['announcement_id' => $announcement_not_published->id, 'group_id' => $group_1->id]);
        $member_announcement_3->save();

        $member_announcement_4 = new MemberAnnouncement(['announcement_id' => $announcement_not_published->id, 'group_id' => $group_2->id]);
        $member_announcement_4->save();

        $announcements = \UserManager::get_current_published_announcements_for_user($user, $admin,);

        $this->assertInstanceOf(Collection::class, $announcements);
        $this->assertEquals($announcements->count(), 1);
    }
}
