<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementCategory;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementCategoryRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementCategoryRepository', 'App\Facades\Repositories\Announcements\AnnouncementCategoryRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementCategoryRepository::test_me();
        $this->assertEquals('announcement_category_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementCategory::factory()->make()->toArray();

        $announcement_category = \AnnouncementCategoryRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $announcement_category);
        $this->assertNotNull($announcement_category->id);
        $this->assertEquals($announcement_category->label, $datas['label']);
        $this->assertEquals($announcement_category->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementCategory::factory()->make()->toArray();

        $announcement_category = \AnnouncementCategoryRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $announcement_category);

        $deleted = \AnnouncementCategoryRepository::delete($announcement_category, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $deleted);
        $this->assertNull(AnnouncementCategory::find($announcement_category->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_categorys = AnnouncementCategory::factory(17)->create();

        $announcement_category = $announcement_categorys->random();

        $found = \AnnouncementCategoryRepository::find($announcement_category->id, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $found);
        $this->assertEquals($announcement_category->id, $found->id);
        $this->assertEquals($announcement_category->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_categorys = AnnouncementCategory::factory(17)->create();

        $announcement_category = $announcement_categorys->random();

        $found = \AnnouncementCategoryRepository::find_by_identity($announcement_category->identity, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $found);
        $this->assertEquals($announcement_category->id, $found->id);
        $this->assertEquals($announcement_category->identity, $found->identity);
        $this->assertEquals($announcement_category->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_categorys = AnnouncementCategory::factory(17)->create();

        $all = \AnnouncementCategoryRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementCategory::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementCategoryRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementCategory::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
