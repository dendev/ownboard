<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementHistoricStatus;
use App\Models\Announcements\AnnouncementStatus;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementHistoricStatusRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementHistoricStatusRepository', 'App\Facades\Repositories\Announcements\AnnouncementHistoricStatusRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementHistoricStatusRepository::test_me();
        $this->assertEquals('announcement_historic_status_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementHistoricStatus::factory()->make()->toArray();

        $announcement_historic_status = \AnnouncementHistoricStatusRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementHistoricStatus::class, $announcement_historic_status);
        $this->assertNotNull($announcement_historic_status->id);
        $this->assertEquals($announcement_historic_status->announcement_id, $datas['announcement_id']);
        $this->assertEquals($announcement_historic_status->status_id, $datas['status_id']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementHistoricStatus::factory()->make()->toArray();

        $announcement_historic_status = \AnnouncementHistoricStatusRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementHistoricStatus::class, $announcement_historic_status);

        $deleted = \AnnouncementHistoricStatusRepository::delete($announcement_historic_status, $admin);

        $this->assertInstanceOf(AnnouncementHistoricStatus::class, $deleted);
        $this->assertNull(AnnouncementHistoricStatus::find($announcement_historic_status->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_historic_statuss = AnnouncementHistoricStatus::factory(17)->create();

        $announcement_historic_status = $announcement_historic_statuss->random();

        $found = \AnnouncementHistoricStatusRepository::find($announcement_historic_status->id, $admin);

        $this->assertInstanceOf(AnnouncementHistoricStatus::class, $found);
        $this->assertEquals($announcement_historic_status->id, $found->id);
        $this->assertEquals($announcement_historic_status->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_historic_statuss = AnnouncementHistoricStatus::factory(17)->create();

        $announcement_historic_status = $announcement_historic_statuss->random();

        $not_found = \AnnouncementHistoricStatusRepository::find_by_identity("don't have an identity", $admin);

        $this->assertFalse($not_found );
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_historic_statuss = AnnouncementHistoricStatus::factory(17)->create();

        $all = \AnnouncementHistoricStatusRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementHistoricStatus::all()->random();

        $new_announcement = Announcement::factory()->create();
        $new_status = AnnouncementStatus::factory()->create();

        $datas = [
            'announcement_id' => $new_announcement->id,
            'status_id' => $new_status->id,
        ];

        $updated = \AnnouncementHistoricStatusRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementHistoricStatus::class, $updated);
        $this->assertEquals($updated->announcement_id, $datas['announcement_id']);
        $this->assertEquals($updated->status_id, $datas['status_id']);
    }
}

