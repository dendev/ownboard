<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementImage;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementImageRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementImageRepository', 'App\Facades\Repositories\Announcements\AnnouncementImageRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementImageRepository::test_me();
        $this->assertEquals('announcement_image_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementImage::factory()->make()->toArray();

        $announcement_image = \AnnouncementImageRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $announcement_image);
        $this->assertNotNull($announcement_image->id);
        $this->assertEquals($announcement_image->label, $datas['label']);
        $this->assertEquals($announcement_image->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementImage::factory()->make()->toArray();

        $announcement_image = \AnnouncementImageRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $announcement_image);

        $deleted = \AnnouncementImageRepository::delete($announcement_image, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $deleted);
        $this->assertNull(AnnouncementImage::find($announcement_image->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_images = AnnouncementImage::factory(17)->create();

        $announcement_image = $announcement_images->random();

        $found = \AnnouncementImageRepository::find($announcement_image->id, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $found);
        $this->assertEquals($announcement_image->id, $found->id);
        $this->assertEquals($announcement_image->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_images = AnnouncementImage::factory(17)->create();

        $announcement_image = $announcement_images->random();

        $found = \AnnouncementImageRepository::find_by_identity($announcement_image->identity, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $found);
        $this->assertEquals($announcement_image->id, $found->id);
        $this->assertEquals($announcement_image->identity, $found->identity);
        $this->assertEquals($announcement_image->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_images = AnnouncementImage::factory(17)->create();

        $all = \AnnouncementImageRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementImage::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementImageRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementImage::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}

