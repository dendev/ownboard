<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementLink;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementLinkRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementLinkRepository', 'App\Facades\Repositories\Announcements\AnnouncementLinkRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementLinkRepository::test_me();
        $this->assertEquals('announcement_link_repository', $exist);
    }


    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementLink::factory()->make()->toArray();

        $announcement_link = \AnnouncementLinkRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $announcement_link);
        $this->assertNotNull($announcement_link->id);
        $this->assertEquals($announcement_link->label, $datas['label']);
        $this->assertEquals($announcement_link->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementLink::factory()->make()->toArray();

        $announcement_link = \AnnouncementLinkRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $announcement_link);

        $deleted = \AnnouncementLinkRepository::delete($announcement_link, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $deleted);
        $this->assertNull(AnnouncementLink::find($announcement_link->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_links = AnnouncementLink::factory(17)->create();

        $announcement_link = $announcement_links->random();

        $found = \AnnouncementLinkRepository::find($announcement_link->id, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $found);
        $this->assertEquals($announcement_link->id, $found->id);
        $this->assertEquals($announcement_link->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_links = AnnouncementLink::factory(17)->create();

        $announcement_link = $announcement_links->random();

        $found = \AnnouncementLinkRepository::find_by_identity($announcement_link->identity, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $found);
        $this->assertEquals($announcement_link->id, $found->id);
        $this->assertEquals($announcement_link->identity, $found->identity);
        $this->assertEquals($announcement_link->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_links = AnnouncementLink::factory(17)->create();

        $all = \AnnouncementLinkRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementLink::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementLinkRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementLink::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
