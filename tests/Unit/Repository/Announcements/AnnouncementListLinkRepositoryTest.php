<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Announcements\AnnouncementLink;
use App\Models\Announcements\AnnouncementListLink;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementListLinkRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementListLinkRepository', 'App\Facades\Repositories\Announcements\AnnouncementListLinkRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementListLinkRepository::test_me();
        $this->assertEquals('announcement_list_link_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementListLink::factory()->make()->toArray();

        $announcement_list_link = \AnnouncementListLinkRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementListLink::class, $announcement_list_link);
        $this->assertNotNull($announcement_list_link->id);
        $this->assertEquals($announcement_list_link->announcement_id, $datas['announcement_id']);
        $this->assertEquals($announcement_list_link->link_id, $datas['link_id']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementListLink::factory()->make()->toArray();

        $announcement_list_link = \AnnouncementListLinkRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementListLink::class, $announcement_list_link);

        $deleted = \AnnouncementListLinkRepository::delete($announcement_list_link, $admin);

        $this->assertInstanceOf(AnnouncementListLink::class, $deleted);
        $this->assertNull(AnnouncementListLink::find($announcement_list_link->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_list_links = AnnouncementListLink::factory(17)->create();

        $announcement_list_link = $announcement_list_links->random();

        $found = \AnnouncementListLinkRepository::find($announcement_list_link->id, $admin);

        $this->assertInstanceOf(AnnouncementListLink::class, $found);
        $this->assertEquals($announcement_list_link->id, $found->id);
        $this->assertEquals($announcement_list_link->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_list_links = AnnouncementListLink::factory(17)->create();

        $announcement_list_link = $announcement_list_links->random();

        $not_found = \AnnouncementListLinkRepository::find_by_identity("don't have an identity", $admin);

        $this->assertFalse($not_found);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_list_links = AnnouncementListLink::factory(17)->create();

        $all = \AnnouncementListLinkRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementListLink::all()->random();

        $new_announcement = Announcement::factory()->create();
        $new_link = AnnouncementLink::factory()->create();

        $datas = [
            'announcement_id' => $new_announcement->id,
            'link_id' => $new_link->id,
        ];

        $updated = \AnnouncementListLinkRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementListLink::class, $updated);
        $this->assertEquals($updated->announcement_id, $datas['announcement_id']);
        $this->assertEquals($updated->link_id, $datas['link_id']);
    }
}
