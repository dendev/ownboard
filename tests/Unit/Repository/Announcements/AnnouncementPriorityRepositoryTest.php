<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementPriority;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementPriorityRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementPriorityRepository', 'App\Facades\Repositories\Announcements\AnnouncementPriorityRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementPriorityRepository::test_me();
        $this->assertEquals('announcement_priority_repository', $exist);
    }


    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementPriority::factory()->make()->toArray();

        $announcement_priority = \AnnouncementPriorityRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $announcement_priority);
        $this->assertNotNull($announcement_priority->id);
        $this->assertEquals($announcement_priority->label, $datas['label']);
        $this->assertEquals($announcement_priority->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementPriority::factory()->make()->toArray();

        $announcement_priority = \AnnouncementPriorityRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $announcement_priority);

        $deleted = \AnnouncementPriorityRepository::delete($announcement_priority, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $deleted);
        $this->assertNull(AnnouncementPriority::find($announcement_priority->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_prioritys = AnnouncementPriority::factory(17)->create();

        $announcement_priority = $announcement_prioritys->random();

        $found = \AnnouncementPriorityRepository::find($announcement_priority->id, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $found);
        $this->assertEquals($announcement_priority->id, $found->id);
        $this->assertEquals($announcement_priority->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_prioritys = AnnouncementPriority::factory(17)->create();

        $announcement_priority = $announcement_prioritys->random();

        $found = \AnnouncementPriorityRepository::find_by_identity($announcement_priority->identity, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $found);
        $this->assertEquals($announcement_priority->id, $found->id);
        $this->assertEquals($announcement_priority->identity, $found->identity);
        $this->assertEquals($announcement_priority->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_prioritys = AnnouncementPriority::factory(17)->create();

        $all = \AnnouncementPriorityRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementPriority::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementPriorityRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementPriority::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
