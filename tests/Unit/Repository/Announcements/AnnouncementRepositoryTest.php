<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\Announcement;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementRepository', 'App\Facades\Repositories\Announcements\AnnouncementRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementRepository::test_me();
        $this->assertEquals('announcement_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = Announcement::factory()->make()->toArray();

        $announcement = \AnnouncementRepository::create($datas, $admin);

        $this->assertInstanceOf(Announcement::class, $announcement);
        $this->assertNotNull($announcement->id);
        $this->assertEquals($announcement->label, $datas['label']);
        $this->assertEquals($announcement->content, $datas['content']);
        $this->assertEquals($announcement->category_id, $datas['category_id']);
        $this->assertEquals($announcement->type_id, $datas['type_id']);
        $this->assertEquals($announcement->status_id, $datas['status_id']);
        $this->assertEquals($announcement->priority_id, $datas['priority_id']);
        $this->assertEquals($announcement->begin_at, $datas['begin_at']);
        $this->assertEquals($announcement->end_at, $datas['end_at']);
        $this->assertEquals($announcement->image_id, $datas['image_id']);
        $this->assertEquals($announcement->creator_id, $datas['creator_id']);
        $this->assertEquals($announcement->corrector_id, $datas['corrector_id']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = Announcement::factory()->make()->toArray();

        $announcement = \AnnouncementRepository::create($datas, $admin);

        $this->assertInstanceOf(Announcement::class, $announcement);

        $deleted = \AnnouncementRepository::delete($announcement, $admin);

        $this->assertInstanceOf(Announcement::class, $deleted);
        $this->assertNull(Announcement::find($announcement->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcements = Announcement::factory(17)->create();

        $announcement = $announcements->random();

        $found = \AnnouncementRepository::find($announcement->id, $admin);

        $this->assertInstanceOf(Announcement::class, $found);
        $this->assertEquals($announcement->id, $found->id);
        $this->assertEquals($announcement->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcements = Announcement::factory(17)->create();

        $announcement = $announcements->random();

        $found = \AnnouncementRepository::find_by_identity($announcement->identity, $admin);

        $this->assertInstanceOf(Announcement::class, $found);
        $this->assertEquals($announcement->id, $found->id);
        $this->assertEquals($announcement->identity, $found->identity);
        $this->assertEquals($announcement->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcements = Announcement::factory(17)->create();

        $all = \AnnouncementRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = Announcement::all()->random();

        $datas = Announcement::factory()->make()->toArray();

        $updated = \AnnouncementRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(Announcement::class, $updated);
        $this->assertNotNull($updated->id);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->content, $datas['content']);
        $this->assertEquals($updated->category_id, $datas['category_id']);
        $this->assertEquals($updated->type_id, $datas['type_id']);
        $this->assertEquals($updated->status_id, $datas['status_id']);
        $this->assertEquals($updated->priority_id, $datas['priority_id']);
        $this->assertEquals($updated->begin_at, $datas['begin_at']);
        $this->assertEquals($updated->end_at, $datas['end_at']);
        $this->assertEquals($updated->image_id, $datas['image_id']);
        $this->assertEquals($updated->creator_id, $datas['creator_id']);
        $this->assertEquals($updated->corrector_id, $datas['corrector_id']);
    }
}

