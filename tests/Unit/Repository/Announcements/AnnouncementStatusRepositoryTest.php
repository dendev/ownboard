<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementStatus;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementStatusRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementStatusRepository', 'App\Facades\Repositories\Announcements\AnnouncementStatusRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementStatusRepository::test_me();
        $this->assertEquals('announcement_status_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementStatus::factory()->make()->toArray();

        $announcement_status = \AnnouncementStatusRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $announcement_status);
        $this->assertNotNull($announcement_status->id);
        $this->assertEquals($announcement_status->label, $datas['label']);
        $this->assertEquals($announcement_status->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementStatus::factory()->make()->toArray();

        $announcement_status = \AnnouncementStatusRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $announcement_status);

        $deleted = \AnnouncementStatusRepository::delete($announcement_status, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $deleted);
        $this->assertNull(AnnouncementStatus::find($announcement_status->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_statuss = AnnouncementStatus::factory(17)->create();

        $announcement_status = $announcement_statuss->random();

        $found = \AnnouncementStatusRepository::find($announcement_status->id, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $found);
        $this->assertEquals($announcement_status->id, $found->id);
        $this->assertEquals($announcement_status->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_statuss = AnnouncementStatus::factory(17)->create();

        $announcement_status = $announcement_statuss->random();

        $found = \AnnouncementStatusRepository::find_by_identity($announcement_status->identity, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $found);
        $this->assertEquals($announcement_status->id, $found->id);
        $this->assertEquals($announcement_status->identity, $found->identity);
        $this->assertEquals($announcement_status->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_statuss = AnnouncementStatus::factory(17)->create();

        $all = \AnnouncementStatusRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementStatus::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementStatusRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementStatus::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}

