<?php

namespace Tests\Unit\Repository\Announcements;

use App\Models\Announcements\AnnouncementType;
use App\Models\Users\User;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class AnnouncementTypeRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\AnnouncementTypeRepository', 'App\Facades\Repositories\Announcements\AnnouncementTypeRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \AnnouncementTypeRepository::test_me();
        $this->assertEquals('announcement_type_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementType::factory()->make()->toArray();

        $announcement_type = \AnnouncementTypeRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $announcement_type);
        $this->assertNotNull($announcement_type->id);
        $this->assertEquals($announcement_type->label, $datas['label']);
        $this->assertEquals($announcement_type->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = AnnouncementType::factory()->make()->toArray();

        $announcement_type = \AnnouncementTypeRepository::create($datas, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $announcement_type);

        $deleted = \AnnouncementTypeRepository::delete($announcement_type, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $deleted);
        $this->assertNull(AnnouncementType::find($announcement_type->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_types = AnnouncementType::factory(17)->create();

        $announcement_type = $announcement_types->random();

        $found = \AnnouncementTypeRepository::find($announcement_type->id, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $found);
        $this->assertEquals($announcement_type->id, $found->id);
        $this->assertEquals($announcement_type->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $announcement_types = AnnouncementType::factory(17)->create();

        $announcement_type = $announcement_types->random();

        $found = \AnnouncementTypeRepository::find_by_identity($announcement_type->identity, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $found);
        $this->assertEquals($announcement_type->id, $found->id);
        $this->assertEquals($announcement_type->identity, $found->identity);
        $this->assertEquals($announcement_type->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $announcement_types = AnnouncementType::factory(17)->create();

        $all = \AnnouncementTypeRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = AnnouncementType::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \AnnouncementTypeRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(AnnouncementType::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}

