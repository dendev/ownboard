<?php

namespace Tests\Unit\Repository\Groups;

use App\Models\Groups\GroupCategory;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class GroupCategoryRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\GroupCategoryRepository', 'App\Facades\Repositories\Groups\GroupCategoryRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \GroupCategoryRepository::test_me();
        $this->assertEquals('group_category_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupCategory::factory()->make()->toArray();

        $group_category = \GroupCategoryRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupCategory::class, $group_category);
        $this->assertNotNull($group_category->id);
        $this->assertEquals($group_category->label, $datas['label']);
        $this->assertEquals($group_category->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupCategory::factory()->make()->toArray();

        $group_category = \GroupCategoryRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupCategory::class, $group_category);

        $deleted = \GroupCategoryRepository::delete($group_category, $admin);

        $this->assertInstanceOf(GroupCategory::class, $deleted);
        $this->assertNull(GroupCategory::find($group_category->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_categorys = GroupCategory::factory(17)->create();

        $group_category = $group_categorys->random();

        $found = \GroupCategoryRepository::find($group_category->id, $admin);

        $this->assertInstanceOf(GroupCategory::class, $found);
        $this->assertEquals($group_category->id, $found->id);
        $this->assertEquals($group_category->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_categorys = GroupCategory::factory(17)->create();

        $group_category = $group_categorys->random();

        $found = \GroupCategoryRepository::find_by_identity($group_category->identity, $admin);

        $this->assertInstanceOf(GroupCategory::class, $found);
        $this->assertEquals($group_category->id, $found->id);
        $this->assertEquals($group_category->identity, $found->identity);
        $this->assertEquals($group_category->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_categorys = GroupCategory::factory(17)->create();

        $all = \GroupCategoryRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = GroupCategory::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \GroupCategoryRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(GroupCategory::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
