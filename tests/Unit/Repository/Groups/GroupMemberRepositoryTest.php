<?php

namespace Tests\Unit\Repository\Groups;

use App\Models\Groups\GroupMember;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class GroupMemberRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\GroupMemberRepository', 'App\Facades\Repositories\Groups\GroupMemberRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \GroupMemberRepository::test_me();
        $this->assertEquals('group_member_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupMember::factory()->make()->toArray();

        $group_member = \GroupMemberRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupMember::class, $group_member);
        $this->assertNotNull($group_member->id);
        $this->assertEquals($group_member->member_id, $datas['member_id']);
        $this->assertEquals($group_member->member_table, $datas['member_table']);
        $this->assertEquals($group_member->group_id, $datas['group_id']);
        $this->assertEquals($group_member->is_user, $datas['is_user']);
        $this->assertEquals($group_member->user_can_view_members, $datas['user_can_view_members']);
        $this->assertEquals($group_member->user_can_remove_member, $datas['user_can_remove_member']);
        $this->assertEquals($group_member->user_can_add_member, $datas['user_can_add_member']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupMember::factory()->make()->toArray();

        $group_member = \GroupMemberRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupMember::class, $group_member);

        $deleted = \GroupMemberRepository::delete($group_member, $admin);

        $this->assertInstanceOf(GroupMember::class, $deleted);
        $this->assertNull(GroupMember::find($group_member->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_members = GroupMember::factory(17)->create();

        $group_member = $group_members->random();

        $found = \GroupMemberRepository::find($group_member->id, $admin);

        $this->assertInstanceOf(GroupMember::class, $found);
        $this->assertEquals($group_member->id, $found->id);
        $this->assertEquals($group_member->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_members = GroupMember::factory(17)->create();

        $group_member = $group_members->random();

        $found = \GroupMemberRepository::find_by_identity('not exist', $admin);

        $this->assertFalse($found);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_members = GroupMember::factory(17)->create();

        $all = \GroupMemberRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = GroupMember::factory()->create([
            'member_id' => 1,
            'member_table' => 'users',
            'group_id' => 1,
            'is_user' => true,
            'user_can_view_members' => true,
            'user_can_remove_member' => true,
            'user_can_add_member' => true,
        ]);

        $datas = [
            'member_id' => 2,
            'member_table' => 'users',
            'group_id' => 2,
            'is_user' => false,
            'user_can_view_members' => false,
            'user_can_remove_member' => false,
            'user_can_add_member' => false,
        ];

        $updated = \GroupMemberRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(GroupMember::class, $updated);
        $this->assertEquals($updated->member_id, $datas['member_id']);
        $this->assertEquals($updated->member_table, $datas['member_table']);
        $this->assertEquals($updated->group_id, $datas['group_id']);
        $this->assertEquals($updated->is_user, $datas['is_user']);
        $this->assertEquals($updated->user_can_view_members, $datas['user_can_view_members']);
        $this->assertEquals($updated->user_can_remove_member, $datas['user_can_remove_member']);
        $this->assertEquals($updated->user_can_add_member, $datas['user_can_add_member']);

    }
}
