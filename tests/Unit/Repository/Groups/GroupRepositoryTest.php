<?php

namespace Tests\Unit\Repository\Groups;

use App\Models\Groups\Group;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class GroupRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\GroupRepository', 'App\Facades\Repositories\Groups\GroupRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \GroupRepository::test_me();
        $this->assertEquals('group_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = Group::factory()->make()->toArray();

        $group = \GroupRepository::create($datas, $admin);

        $this->assertInstanceOf(Group::class, $group);
        $this->assertNotNull($group->id);
        $this->assertEquals($group->label, $datas['label']);
        $this->assertEquals($group->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = Group::factory()->make()->toArray();

        $group = \GroupRepository::create($datas, $admin);

        $this->assertInstanceOf(Group::class, $group);

        $deleted = \GroupRepository::delete($group, $admin);

        $this->assertInstanceOf(Group::class, $deleted);
        $this->assertNull(Group::find($group->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $groups = Group::factory(17)->create();

        $group = $groups->random();

        $found = \GroupRepository::find($group->id, $admin);

        $this->assertInstanceOf(Group::class, $found);
        $this->assertEquals($group->id, $found->id);
        $this->assertEquals($group->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $groups = Group::factory(17)->create();

        $group = $groups->random();

        $found = \GroupRepository::find_by_identity($group->identity, $admin);

        $this->assertInstanceOf(Group::class, $found);
        $this->assertEquals($group->id, $found->id);
        $this->assertEquals($group->identity, $found->identity);
        $this->assertEquals($group->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $groups = Group::factory(17)->create();

        $all = \GroupRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = Group::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \GroupRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(Group::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
