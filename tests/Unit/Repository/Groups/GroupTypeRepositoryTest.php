<?php

namespace Tests\Unit\Repository\Groups;

use App\Models\Groups\GroupType;
use App\Models\Users\User;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Tests\TestCase;


class GroupTypeRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\GroupTypeRepository', 'App\Facades\Repositories\Groups\GroupTypeRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \GroupTypeRepository::test_me();
        $this->assertEquals('group_type_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupType::factory()->make()->toArray();

        $group_type = \GroupTypeRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupType::class, $group_type);
        $this->assertNotNull($group_type->id);
        $this->assertEquals($group_type->label, $datas['label']);
        $this->assertEquals($group_type->description, $datas['description']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = GroupType::factory()->make()->toArray();

        $group_type = \GroupTypeRepository::create($datas, $admin);

        $this->assertInstanceOf(GroupType::class, $group_type);

        $deleted = \GroupTypeRepository::delete($group_type, $admin);

        $this->assertInstanceOf(GroupType::class, $deleted);
        $this->assertNull(GroupType::find($group_type->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_types = GroupType::factory(17)->create();

        $group_type = $group_types->random();

        $found = \GroupTypeRepository::find($group_type->id, $admin);

        $this->assertInstanceOf(GroupType::class, $found);
        $this->assertEquals($group_type->id, $found->id);
        $this->assertEquals($group_type->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $group_types = GroupType::factory(17)->create();

        $group_type = $group_types->random();

        $found = \GroupTypeRepository::find_by_identity($group_type->identity, $admin);

        $this->assertInstanceOf(GroupType::class, $found);
        $this->assertEquals($group_type->id, $found->id);
        $this->assertEquals($group_type->identity, $found->identity);
        $this->assertEquals($group_type->label, $found->label);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_types = GroupType::factory(17)->create();

        $all = \GroupTypeRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = GroupType::all()->random();

        $datas = [
            'label' => 'My Label',
            'description' => 'my short description'
        ];

        $updated = \GroupTypeRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(GroupType::class, $updated);
        $this->assertEquals($updated->label, $datas['label']);
        $this->assertEquals($updated->description, $datas['description']);

    }
}
