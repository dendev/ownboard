<?php

namespace Tests\Unit\Repository\Members;

use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class MemberAnnouncementRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\MemberAnnouncementRepository', 'App\Facades\Repositories\Members\MemberAnnouncementRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \MemberAnnouncementRepository::test_me();
        $this->assertEquals('member_announcement_repository', $exist);
    }
}
