<?php

namespace Tests\Unit\Repository\Members;

use App\Models\Groups\Group;
use App\Models\Members\MemberUser;
use App\Models\Users\User;
use Database\Factories\Users\UserFactory;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class MemberUserRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\MemberUserRepository', 'App\Facades\Repositories\Members\MemberUserRepositoryFacade');
     }

    public function test_basic(): void
    {
        $exist = \MemberUserRepository::test_me();
        $this->assertEquals('member_user_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = MemberUser::factory()->make()->toArray();
        $datas['user_id'] = $datas['member_id'];

        $member_user = \MemberUserRepository::create($datas, $admin);

        $this->assertInstanceOf(MemberUser::class, $member_user);
        $this->assertNotNull($member_user->id);
        $this->assertEquals($member_user->user_id, $datas['user_id']);
        $this->assertEquals($member_user->group_id, $datas['group_id']);
        $this->assertEquals($member_user->is_user, $datas['is_user']);
        $this->assertEquals($member_user->user_can_view_members, $datas['user_can_view_members']);
        $this->assertEquals($member_user->user_can_remove_member, $datas['user_can_remove_member']);
        $this->assertEquals($member_user->user_can_add_member, $datas['user_can_add_member']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = MemberUser::factory()->make()->toArray();

        $member_user = \MemberUserRepository::create($datas, $admin);

        $this->assertInstanceOf(MemberUser::class, $member_user);

        $deleted = \MemberUserRepository::delete($member_user, $admin);

        $this->assertInstanceOf(MemberUser::class, $deleted);
        $this->assertNull(MemberUser::find($member_user->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $member_users = MemberUser::factory(17)->create();

        $member_user = $member_users->random();

        $found = \MemberUserRepository::find($member_user->id, $admin);

        $this->assertInstanceOf(MemberUser::class, $found);
        $this->assertEquals($member_user->id, $found->id);
        $this->assertEquals($member_user->label, $found->label);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $member_users = MemberUser::factory(17)->create();

        $member_user = $member_users->random();

        $found = \MemberUserRepository::find_by_identity('not exist', $admin);

        $this->assertFalse($found);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $member_users = MemberUser::factory(17)->create();

        $all = \MemberUserRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = MemberUser::factory()->create([
            'user_id' => 1,
            'group_id' => 1,
            'is_user' => true,
            'user_can_view_members' => true,
            'user_can_remove_member' => true,
            'user_can_add_member' => true,
        ]);

        $datas = [
            'user_id' => 2,
            'group_id' => 2,
            'is_user' => false,
            'user_can_view_members' => false,
            'user_can_remove_member' => false,
            'user_can_add_member' => false,
        ];

        $updated = \MemberUserRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(MemberUser::class, $updated);
        $this->assertEquals($updated->user_id, $datas['user_id']);
        $this->assertEquals($updated->group_id, $datas['group_id']);
        $this->assertEquals($updated->is_user, $datas['is_user']);
        $this->assertEquals($updated->user_can_view_members, $datas['user_can_view_members']);
        $this->assertEquals($updated->user_can_remove_member, $datas['user_can_remove_member']);
        $this->assertEquals($updated->user_can_add_member, $datas['user_can_add_member']);

    }

    public function testGetGroups()
    {
        $this->seed();

        $user = User::factory()->create();
        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $groups = \MemberUserRepository::get_groups($user, $admin);

        $this->assertInstanceOf(Collection::class, $groups);
        $this->assertEquals(2, $groups->count());

        $group_ids = $groups->pluck('id');
        $this->assertContains($group_1->id, $group_ids);
        $this->assertContains($group_2->id, $group_ids);
    }

    public function testGetGroupsWithoutPermission()
    {
        $this->seed();

        $user = User::factory()->create();
        $not_admin = User::factory()->create();

        $group_1 = Group::all()->random();
        $group_2 = Group::all()->random();

        $member_user_1 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_1->id]);
        $member_user_1->save();

        $member_user_2 = new MemberUser(['user_id' => $user->id, 'group_id' => $group_2->id]);
        $member_user_2->save();

        $groups = \MemberUserRepository::get_groups($user, $not_admin);

        $this->assertInstanceOf(Collection::class, $groups);
        $this->assertTrue($groups->isEmpty());
    }
}
