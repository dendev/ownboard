<?php

namespace Tests\Unit\Repository\Users;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;


class UserRepositoryTest extends TestCase
{
    use DatabaseMigrations;

     public function __construct($name = null, array $data = [], $dataName = '')
     {
         parent::__construct($name, $data, $dataName);

         $loader = AliasLoader::getInstance();
         $loader->alias('\UserRepository', 'Tests\Unit\Repository'); // ??
     }

    public function test_basic(): void
    {
        $exist = \UserRepository::test_me();
        $this->assertEquals('user_repository', $exist);
    }

    public function testCreate(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = User::factory()->make()->toArray();
        $datas['password'] = Hash::make('test');

        $user = \UserRepository::create($datas, $admin);

        $this->assertInstanceOf(User::class, $user);
        $this->assertNotNull($user->id);
        $this->assertEquals($user->name, $datas['name']);
        $this->assertEquals($user->email, $datas['email']);
    }

    public function testDelete(): void
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $datas = User::factory()->make()->toArray();
        $datas['password'] = Hash::make('test');

        $user = \UserRepository::create($datas, $admin);

        $this->assertInstanceOf(User::class, $user);

        $deleted = \UserRepository::delete($user, $admin);

        $this->assertInstanceOf(User::class, $deleted);
        $this->assertNull(User::find($user->id));
    }

    public function testFind()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $users = User::factory(17)->create();

        $user = $users->random();

        $found = \UserRepository::find($user->id, $admin);

        $this->assertInstanceOf(User::class, $found);
        $this->assertEquals($user->id, $found->id);
        $this->assertEquals($user->email, $found->email);
    }

    public function testFindByIdentity()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();
        $users = User::factory(17)->create();

        $user = $users->random();

        $found = \UserRepository::find_by_identity($user->identity, $admin);

        $this->assertInstanceOf(User::class, $found);
        $this->assertEquals($user->id, $found->id);
        $this->assertEquals($user->identity, $found->identity);
        $this->assertEquals($user->email, $found->email);
    }

    public function testGetAll()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $all = \UserRepository::get_all($admin);

        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testUpdate()
    {
        $this->seed();

        $admin = User::where('email', env('ADMIN_EMAIL'))->first();

        $model = User::all()->random();

        $datas = [
            'name' => 'My test',
            'email' => 'myemail@gmail.com'
        ];

        $updated = \UserRepository::update($model, $datas, $admin);

        $this->assertInstanceOf(User::class, $updated);
        $this->assertEquals($updated->name, $datas['name']);
        $this->assertEquals($updated->email, $datas['email']);

    }
}
